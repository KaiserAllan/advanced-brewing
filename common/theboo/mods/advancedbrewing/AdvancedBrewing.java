package theboo.mods.advancedbrewing;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

import net.minecraftforge.common.Configuration;
import net.minecraftforge.common.MinecraftForge;
import theboo.mods.advancedbrewing.block.ModBlocks;
import theboo.mods.advancedbrewing.configuration.ConfigurationLoader;
import theboo.mods.advancedbrewing.entity.ModEntities;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.PotionRegistry;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.proxy.CommonProxy;
import theboo.mods.advancedbrewing.tileentity.ModTileEntities;
import theboo.mods.advancedbrewing.util.ModInfo;
import theboo.mods.advancedbrewing.util.handler.AdvancedBrewingEventHooks;
import theboo.mods.advancedbrewing.util.handler.GuiHandler;
import theboo.mods.advancedbrewing.util.handler.packets.PacketHandler;
import theboo.mods.advancedbrewing.worldgen.WorldGenerator;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;

/**
 * AdvancedBrewing AdvancedBrewing
 *  
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * @author TheBoo
 *   
 */

@Mod(modid = ModInfo.MOD_ID, name = ModInfo.MOD_NAME, version = ModInfo.VERSION_NUMBER, dependencies = ModInfo.DEPENDENCIES)
@NetworkMod(channels = {ModInfo.CHANNEL_NAME}, clientSideRequired = true, serverSideRequired = false, packetHandler = PacketHandler.class)
public class AdvancedBrewing {

	@SidedProxy(clientSide = ModInfo.CLIENT_PROXY_CLASS, serverSide = ModInfo.SERVER_PROXY_CLASS)
	public static CommonProxy proxy;
	
	@Instance(ModInfo.MOD_ID)
	public static AdvancedBrewing instance;
	
	public static Configuration config;
	
	public static ConfigurationLoader loader = new ConfigurationLoader();
	
	public PotionHelper2 helper = new PotionHelper2();

	public static Logger logger = Logger.getLogger("AdvancedBrewing");
	@EventHandler
	public void preInit(final FMLPreInitializationEvent fml) {
		config = new Configuration(fml.getSuggestedConfigurationFile());
		loader.readConfig(config);
		ModBlocks.init();
		ModItems.init();
		ModEntities.init();
		ModTileEntities.init();
		PotionRegistry.addPotions();
		MinecraftForge.EVENT_BUS.register(new AdvancedBrewingEventHooks());
		NetworkRegistry.instance().registerGuiHandler(this, new GuiHandler());	
		GameRegistry.registerWorldGenerator(new WorldGenerator());
		
	}
	
	@EventHandler
	public void init(final FMLInitializationEvent fml) {
		ModItems.addItemRecipes();
		ModBlocks.addBlockRecipes();
		proxy.registerRenderers();
		PotionRegistry.addPotionRecipes();
	}
	
	@EventHandler
	public void postInit(final FMLPostInitializationEvent fml) {
	}
	
	@EventHandler
	public void serverStarting(final FMLServerStartingEvent fml) {
	}
	
	public static URL getUpdateURL() {
		try {
			return new URL("http://pastebin.com/raw.php?i=aptgZ8kt");
		}
		catch(MalformedURLException ex) {
			System.out.println("Woops, URL was wrong!");
			return null;
		}
	}

	public static void logAndPrint(Level level, String string) {
		System.out.println(string);
		logger.log(level, string);
	}
}
