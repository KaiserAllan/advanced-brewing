package theboo.mods.advancedbrewing.core;

import theboo.mods.advancedbrewing.util.ModInfo;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;

/**
 * AdvancedBrewing ItemBrew
 * 
 * Holds some properties for all the AB items.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
 * @author TheBoo
 *
 */
public class ItemBrew extends Item {

	public ItemBrew(int par1) {
		super(par1);
	}
	
	public void registerIcons(IconRegister reg) {
		this.itemIcon = reg.registerIcon(ModInfo.MOD_ID + ":" + this.getUnlocalizedName().substring(5));
	}

}
