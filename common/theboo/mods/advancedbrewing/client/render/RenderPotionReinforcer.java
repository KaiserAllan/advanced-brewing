package theboo.mods.advancedbrewing.client.render;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

import theboo.mods.advancedbrewing.client.model.ModelPotionReinforcer;
import theboo.mods.advancedbrewing.util.ModInfo;

public class RenderPotionReinforcer extends TileEntitySpecialRenderer {

	private ModelPotionReinforcer model;

    public RenderPotionReinforcer() {
    	model = new ModelPotionReinforcer();
    }

	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float f) {
		this.renderModel(tileentity, x, y, z);
	}

	private void renderModel(TileEntity tileentity,double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        bindTexture(new ResourceLocation(ModInfo.MOD_ID + ":textures/blocks/reinforcer.png")); 
        GL11.glPushMatrix();
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        this.model.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
        GL11.glPopMatrix();
	}
	
	
}
