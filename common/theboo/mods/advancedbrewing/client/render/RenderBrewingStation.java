package theboo.mods.advancedbrewing.client.render;

import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;

import org.lwjgl.opengl.GL11;

import theboo.mods.advancedbrewing.block.BlockBrewingStation;
import theboo.mods.advancedbrewing.client.model.ModelBrewingStation;
import theboo.mods.advancedbrewing.client.model.ModelBrewingTable;
import theboo.mods.advancedbrewing.tileentity.TileEntityBrewingStation;
import theboo.mods.advancedbrewing.util.ModInfo;

public class RenderBrewingStation extends TileEntitySpecialRenderer {

    private ModelBrewingStation modelConnected;
	private ModelBrewingTable modelSingle;

    public RenderBrewingStation() {
    	modelConnected = new ModelBrewingStation();
    	modelSingle = new ModelBrewingTable();
    }

	public void renderTileEntityAt(TileEntity tileentity, double x, double y, double z, float f) {
        BlockBrewingStation block = (BlockBrewingStation) tileentity.blockType;
        World world = tileentity.worldObj;
        
        int ix = (int)x; 
        int iy = (int) y; 
        int iz = (int) z;
        
        switch(block.getConnnectedSide(world, ix, iy, iz)) {
        case 0:
        	renderSingleTable((TileEntityBrewingStation)tileentity, x, y, z);
        	break;
        case 1:
            renderConnectedBrewingStation((TileEntityBrewingStation)tileentity, x, y, z, 1);
            break;
        case 2:
            renderConnectedBrewingStation((TileEntityBrewingStation)tileentity, x, y, z, 2);
            break;
        case 3:
            renderConnectedBrewingStation((TileEntityBrewingStation)tileentity, x, y, z, 3);
            break;
        case 4:
            renderConnectedBrewingStation((TileEntityBrewingStation)tileentity, x, y, z, 4);
            break;
        }
    }

	private void renderSingleTable(TileEntityBrewingStation tileentity,double x, double y, double z) {
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        bindTexture(new ResourceLocation(ModInfo.MOD_ID + ":textures/blocks/brewing_table.png")); 
        GL11.glPushMatrix();
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        this.modelSingle.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
        GL11.glPopMatrix();
        GL11.glPopMatrix();
	}
	
    private void renderConnectedBrewingStation(TileEntityBrewingStation tileentity1, double x, double y, double z, int side) {  
        GL11.glPushMatrix();
        GL11.glTranslatef((float) x + 0.5F, (float) y + 1.5F, (float) z + 0.5F);
        bindTexture(new ResourceLocation(ModInfo.MOD_ID + ":textures/blocks/brewing_station.png")); 
        GL11.glPushMatrix();
        GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
        
        if(side == 1) this.modelConnected.render((Entity)null, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
        else if(side == 2) this.modelConnected.render((Entity)null, 0.0F, 0.0F, +0.1F, 0.0F, 0.0F, 0.0625F);
        else if(side == 3) this.modelConnected.render((Entity)null, 0.0F, 0.0F, 0.1F, 0.0F, -0.1F, 0.0625F);
        else if(side == 4) this.modelConnected.render((Entity)null, 0.0F, 0.0F, 0.1F, 0.0F, +0.1F, 0.0625F);

        GL11.glPopMatrix();
        GL11.glPopMatrix();
    }
	
}
