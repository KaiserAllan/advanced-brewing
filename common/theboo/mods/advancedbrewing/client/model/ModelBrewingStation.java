package theboo.mods.advancedbrewing.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelBrewingStation extends ModelBase {
	// fields
	ModelRenderer bottomPlate;
	ModelRenderer topPlate;
	ModelRenderer leg1;
	ModelRenderer leg2;
	ModelRenderer leg3;
	ModelRenderer leg4;
	ModelRenderer leg5;
	ModelRenderer leg6;
	ModelRenderer leg7;
	ModelRenderer leg8;

	public ModelBrewingStation() {
		textureWidth = 128;
		textureHeight = 64;

		bottomPlate = new ModelRenderer(this, 0, 19);
		bottomPlate.addBox(0F, 0F, 0F, 32, 3, 16);
		bottomPlate.setRotationPoint(-8F, 18F, -8F);
		bottomPlate.setTextureSize(128, 64);
		bottomPlate.mirror = true;
		setRotation(bottomPlate, 0F, 0F, 0F);
		topPlate = new ModelRenderer(this, 0, 0);
		topPlate.addBox(0F, 0F, 0F, 32, 3, 16);
		topPlate.setRotationPoint(-8F, 8F, -8F);
		topPlate.setTextureSize(128, 64);
		topPlate.mirror = true;
		setRotation(topPlate, 0F, 0F, 0F);
		leg1 = new ModelRenderer(this, 96, 0);
		leg1.addBox(0F, 0F, 0F, 3, 7, 3);
		leg1.setRotationPoint(-8F, 11F, 5F);
		leg1.setTextureSize(128, 64);
		leg1.mirror = true;
		setRotation(leg1, 0F, 0F, 0F);
		leg2 = new ModelRenderer(this, 96, 0);
		leg2.addBox(0F, 0F, 0F, 3, 7, 3);
		leg2.setRotationPoint(-8F, 11F, -8F);
		leg2.setTextureSize(128, 64);
		leg2.mirror = true;
		setRotation(leg2, 0F, 0F, 0F);
		leg3 = new ModelRenderer(this, 108, 0);
		leg3.addBox(0F, 0F, 0F, 3, 7, 3);
		leg3.setRotationPoint(21F, 11F, -8F);
		leg3.setTextureSize(128, 64);
		leg3.mirror = true;
		setRotation(leg3, 0F, 0F, 0F);
		leg4 = new ModelRenderer(this, 108, 0);
		leg4.addBox(0F, 0F, 0F, 3, 7, 3);
		leg4.setRotationPoint(21F, 11F, 5F);
		leg4.setTextureSize(128, 64);
		leg4.mirror = true;
		setRotation(leg4, 0F, 0F, 0F);
		leg5 = new ModelRenderer(this, 96, 10);
		leg5.addBox(0F, 0F, 0F, 3, 3, 3);
		leg5.setRotationPoint(-8F, 21F, -8F);
		leg5.setTextureSize(128, 64);
		leg5.mirror = true;
		setRotation(leg5, 0F, 0F, 0F);
		leg6 = new ModelRenderer(this, 108, 10);
		leg6.addBox(0F, 0F, 0F, 3, 3, 3);
		leg6.setRotationPoint(21F, 21F, -8F);
		leg6.setTextureSize(128, 64);
		leg6.mirror = true;
		setRotation(leg6, 0F, 0F, 0F);
		leg7 = new ModelRenderer(this, 96, 10);
		leg7.addBox(0F, 0F, 0F, 3, 3, 3);
		leg7.setRotationPoint(-8F, 21F, 5F);
		leg7.setTextureSize(128, 64);
		leg7.mirror = true;
		setRotation(leg7, 0F, 0F, 0F);
		leg8 = new ModelRenderer(this, 108, 10);
		leg8.addBox(0F, 0F, 0F, 3, 3, 3);
		leg8.setRotationPoint(21F, 21F, 5F);
		leg8.setTextureSize(128, 64);
		leg8.mirror = true;
		setRotation(leg8, 0F, 0F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3,float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		bottomPlate.render(f5);
		topPlate.render(f5);
		leg1.render(f5);
		leg2.render(f5);
		leg3.render(f5);
		leg4.render(f5);
		leg5.render(f5);
		leg6.render(f5);
		leg7.render(f5);
		leg8.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3,float f4, float f5) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, null);
	}

}
