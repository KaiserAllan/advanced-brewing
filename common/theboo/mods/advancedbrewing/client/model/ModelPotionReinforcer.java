package theboo.mods.advancedbrewing.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelPotionReinforcer extends ModelBase {
	// fields
	ModelRenderer poke;
	ModelRenderer p1;
	ModelRenderer p2;
	ModelRenderer p3;
	ModelRenderer p4;
	ModelRenderer p5;
	ModelRenderer p6;

	public ModelPotionReinforcer() {
		textureWidth = 64;
		textureHeight = 32;

		poke = new ModelRenderer(this, 0, 4);
		poke.addBox(0F, 0F, 0F, 8, 16, 8);
		poke.setRotationPoint(-4F, 8F, -4F);
		poke.setTextureSize(64, 32);
		poke.mirror = true;
		setRotation(poke, 0F, 0F, 0F);
		p1 = new ModelRenderer(this, 0, 0);
		p1.addBox(0F, 0F, 0F, 16, 2, 1);
		p1.setRotationPoint(-8F, 8F, 8F);
		p1.setTextureSize(64, 32);
		p1.mirror = true;
		setRotation(p1, 0F, 1.570796F, 0F);
		p2 = new ModelRenderer(this, 0, 0);
		p2.addBox(0F, 0F, 0F, 16, 2, 1);
		p2.setRotationPoint(-8F, 20F, 7F);
		p2.setTextureSize(64, 32);
		p2.mirror = true;
		setRotation(p2, 0F, 1.570796F, 0F);
		p3 = new ModelRenderer(this, 0, 0);
		p3.addBox(0F, 0F, 0F, 16, 3, 1);
		p3.setRotationPoint(-8F, 14F, 7F);
		p3.setTextureSize(64, 32);
		p3.mirror = true;
		setRotation(p3, 0F, 0F, 0F);
		p4 = new ModelRenderer(this, 0, 0);
		p4.addBox(0F, 0F, 0F, 16, 3, 1);
		p4.setRotationPoint(-8F, 14F, -8F);
		p4.setTextureSize(64, 32);
		p4.mirror = true;
		setRotation(p4, 0F, 0F, 0F);
		p5 = new ModelRenderer(this, 0, 0);
		p5.addBox(-16F, 0F, 0F, 16, 2, 1);
		p5.setRotationPoint(7F, 8F, -8F);
		p5.setTextureSize(64, 32);
		p5.mirror = true;
		setRotation(p5, 0F, 1.570796F, 0F);
		p6 = new ModelRenderer(this, 0, 0);
		p6.addBox(-16F, 0F, 0F, 16, 2, 1);
		p6.setRotationPoint(7F, 20F, -8F);
		p6.setTextureSize(64, 32);
		p6.mirror = true;
		setRotation(p6, 0F, 1.570796F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3,
			float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		poke.render(f5);
		p1.render(f5);
		p2.render(f5);
		p3.render(f5);
		p4.render(f5);
		p5.render(f5);
		p6.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3,
			float f4, float f5) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, (Entity) null);
	}

}
