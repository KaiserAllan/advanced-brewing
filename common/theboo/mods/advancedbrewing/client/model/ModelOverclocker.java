package theboo.mods.advancedbrewing.client.model;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelOverclocker extends ModelBase {
	ModelRenderer gate1;
	ModelRenderer gate2;
	ModelRenderer gate3;
	ModelRenderer accelerator;
	ModelRenderer backup1;
	ModelRenderer bachup2;

	public ModelOverclocker() {
		textureWidth = 64;
		textureHeight = 32;

		gate1 = new ModelRenderer(this, 0, 0);
		gate1.addBox(0F, 0F, 0F, 6, 6, 2);
		gate1.setRotationPoint(-3F, 16F, -1F);
		gate1.setTextureSize(64, 32);
		gate1.mirror = true;
		setRotation(gate1, 0F, 0F, 0F);
		gate2 = new ModelRenderer(this, 0, 0);
		gate2.addBox(0F, 0F, 0F, 6, 6, 2);
		gate2.setRotationPoint(-3F, 16F, 3F);
		gate2.setTextureSize(64, 32);
		gate2.mirror = true;
		setRotation(gate2, 0F, 0F, 0F);
		gate3 = new ModelRenderer(this, 0, 0);
		gate3.addBox(0F, 0F, 0F, 6, 6, 2);
		gate3.setRotationPoint(-3F, 16F, -5F);
		gate3.setTextureSize(64, 32);
		gate3.mirror = true;
		setRotation(gate3, 0F, 0F, 0F);
		accelerator = new ModelRenderer(this, 28, 0);
		accelerator.addBox(0F, 0F, 0F, 2, 2, 16);
		accelerator.setRotationPoint(-1F, 18F, -8F);
		accelerator.setTextureSize(64, 32);
		accelerator.mirror = true;
		setRotation(accelerator, 0F, 0F, 0F);
		backup1 = new ModelRenderer(this, 0, 20);
		backup1.addBox(0F, 0F, 0F, 14, 10, 2);
		backup1.setRotationPoint(-7F, 14F, 6F);
		backup1.setTextureSize(64, 32);
		backup1.mirror = true;
		setRotation(backup1, 0F, 0F, 0F);
		bachup2 = new ModelRenderer(this, 32, 20);
		bachup2.addBox(0F, 0F, 0F, 14, 10, 2);
		bachup2.setRotationPoint(-7F, 14F, -8F);
		bachup2.setTextureSize(64, 32);
		bachup2.mirror = true;
		setRotation(bachup2, 0F, 0F, 0F);
	}

	public void render(Entity entity, float f, float f1, float f2, float f3,float f4, float f5) {
		super.render(entity, f, f1, f2, f3, f4, f5);
		setRotationAngles(f, f1, f2, f3, f4, f5);
		gate1.render(f5);
		gate2.render(f5);
		gate3.render(f5);
		accelerator.render(f5);
		backup1.render(f5);
		bachup2.render(f5);
	}

	private void setRotation(ModelRenderer model, float x, float y, float z) {
		model.rotateAngleX = x;
		model.rotateAngleY = y;
		model.rotateAngleZ = z;
	}

	public void setRotationAngles(float f, float f1, float f2, float f3, float f4, float f5) {
		super.setRotationAngles(f, f1, f2, f3, f4, f5, (Entity) null);
	}

}
