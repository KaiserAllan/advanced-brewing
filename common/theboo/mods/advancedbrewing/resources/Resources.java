package theboo.mods.advancedbrewing.resources;

import net.minecraft.util.ResourceLocation;
import theboo.mods.advancedbrewing.util.ModInfo;

/**
 * AdvancedBrewing Resources
 * 
 * Previosly held the resources of the mod. May be used again.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
@Deprecated
public class Resources {

	public static final ResourceLocation POTION_ITEM_LOC = new ResourceLocation(
			ModInfo.MOD_ID, "textures/items/potion_bottle_splash.png");
}
