package theboo.mods.advancedbrewing.proxy;

import theboo.mods.advancedbrewing.client.render.RenderBrewingStation;
import theboo.mods.advancedbrewing.client.render.RenderPotionReinforcer;
import theboo.mods.advancedbrewing.entity.EntityPotion2;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.render.RenderPotion;
import theboo.mods.advancedbrewing.tileentity.TileEntityBrewingStation;
import theboo.mods.advancedbrewing.tileentity.TileEntityPotionReinforcer;
import cpw.mods.fml.client.registry.ClientRegistry;
import cpw.mods.fml.client.registry.RenderingRegistry;

/**
 * AdvancedBrewing ClientProxy
 * 
 * <br> Handles the client-side code.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class ClientProxy extends CommonProxy {
            
	public void registerRenderers() {
		RenderingRegistry.registerEntityRenderingHandler(EntityPotion2.class, new RenderPotion(ModItems.potion, 4));
		 
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityBrewingStation.class, new RenderBrewingStation());
		ClientRegistry.bindTileEntitySpecialRenderer(TileEntityPotionReinforcer.class, new RenderPotionReinforcer());

	}
	
}
