package theboo.mods.advancedbrewing.util;

public class ItemIdReference {

	public static int ITEM_ADVANCED_BREWING_STAND;
	public static int ITEM_BREWING_STATION;
	public static int ITEM_POTION_REINFORCER;
	public static int ITEM_POTION_BINDER;

	public static int POTION;
	public static int ROTTEN_FRENCHIE;
	public static int SLIME_BINDED;
	public static int CONSUMED_SOUL;
	public static int MAGICAL_DUST;
	public static int CRYSTAL_QUARTZ;
	public static int SULFUREO;

	public static int ITEM_ADVANCED_BREWING_STAND_DEFAULT = 22501;
	public static int ITEM_BREWING_STATION_DEFAULT = 22502;
	public static int POTION_DEFAULT = 22503;
	public static int ROTTEN_FRENCHIE_DEFAULT = 22504;
	public static int ITEM_POTION_BINDER_DEFAULT = 22505;
	public static int SLIME_BINDED_DEFAULT = 22506;
	public static int ITEM_POTION_REINFORCER_DEFAULT = 22507;
	public static int CONSUMED_SOUL_DEFAULT = 22508;
	public static int MAGICAL_DUST_DEFAULT = 22509;
	public static int CRYSTAL_QUARTZ_DEFAULT = 22510;
	public static int SULFUREI_DEFAULT = 22511;

}

