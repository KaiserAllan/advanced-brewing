package theboo.mods.advancedbrewing.util;

public class BlockIdReference {

	public static int ADVANCED_BREWING_STAND_DEFAULT = 1096;
	public static int BREWING_STATION_DEFAULT = 1097;
	public static int POTION_BINDER_DEFAULT = 1098;
	public static int POTION_REINFORCER_DEFAULT = 1099;
	public static int CRYSTAL_QUARTZ_ORE_DEFAULT = 1100;

	public static int ADVANCED_BREWING_STAND;
	public static int BREWING_STATION;
	public static int POTION_BINDER;
	public static int POTION_REINFORCER;
	public static int CRYSTAL_QUARTZ_ORE;
}
