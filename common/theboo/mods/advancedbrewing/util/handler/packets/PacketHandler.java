package theboo.mods.advancedbrewing.util.handler.packets;

import java.io.ByteArrayOutputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.IOException;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.tileentity.TileEntityElectrized;
import theboo.mods.advancedbrewing.util.ModInfo;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import cpw.mods.fml.common.FMLCommonHandler;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.PacketDispatcher;
import cpw.mods.fml.common.network.Player;

public class PacketHandler implements IPacketHandler {

	public static void sendItemPacket(ItemStack stack, int slotIndex, boolean valid, int x, int y, int z, int dimId) {
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();		
		DataOutputStream dataStream = new DataOutputStream(byteStream);

		try {
			dataStream.writeByte(0);
			dataStream.writeInt(stack.itemID);
			dataStream.writeInt(stack.getItemDamage());
			dataStream.writeInt(stack.stackSize);
			dataStream.writeInt(slotIndex);
			dataStream.writeBoolean(valid);
			
			dataStream.writeInt(x);
			dataStream.writeInt(y);
			dataStream.writeInt(z);
			
			System.out.println("excellent");

			PacketDispatcher.sendPacketToAllAround(x, y, z, 10, dimId, PacketDispatcher.getPacket(ModInfo.CHANNEL_NAME, byteStream.toByteArray()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onPacketData(INetworkManager manager,Packet250CustomPayload packet, Player player) {
		ByteArrayDataInput reader = ByteStreams.newDataInput(packet.data);
		
		byte packetId = reader.readByte();
		
		if(packetId==0) {
			int itemId = reader.readInt();
			int damage = reader.readInt();
			int stackSize = reader.readInt();
			int slotIndex = reader.readInt();
			boolean valid = reader.readBoolean();
			
			int x = reader.readInt();
			int y = reader.readInt();
			int z = reader.readInt();

			System.out.println("Processing yes");
			
			if(FMLCommonHandler.instance().getSide().isServer()) {
				EntityPlayerMP ep = (EntityPlayerMP) player;
				World world = ep.worldObj;
				
				if(world.getBlockTileEntity(x, y, z) != null && world.getBlockTileEntity(x, y, z) instanceof TileEntityElectrized) {
					TileEntityElectrized tileBrewer = (TileEntityElectrized) world.getBlockTileEntity(x, y, z);
					
					tileBrewer.setInventorySlotContents(slotIndex, new ItemStack(itemId, stackSize, damage));
					//ep.sendSlotContents(par1Container, par2, par3ItemStack);
					System.out.println("EYYYYY");
				}
			}
			else {
				EntityPlayer ep = (EntityPlayer) player;
				World world = ep.worldObj;
				
				if(world.getBlockTileEntity(x, y, z) != null && world.getBlockTileEntity(x, y, z) instanceof TileEntityElectrized) {
					TileEntityElectrized tileBrewer = (TileEntityElectrized) world.getBlockTileEntity(x, y, z);
					
					//tileBrewer.setInventorySlotContents(slotIndex, new ItemStack(itemId, stackSize, damage));
					//ep.sendSlotContents(ep.get, par2, par3ItemStack);
					
					System.out.println("EYYYYY");
				}
			}

			System.out.println("hmmm");
		} else if(packetId==1){
			boolean valid = reader.readBoolean();
			
			if(valid) {
				int itemId = reader.readInt();
				int damage = reader.readInt();
				int stackSize = reader.readInt();
				int slotIndex = reader.readInt();
				int dimId = reader.readInt();

				int x = reader.readInt();
				int y = reader.readInt();
				int z = reader.readInt();
				
				PacketHandler.sendItemPacket(new ItemStack(itemId, stackSize, damage), slotIndex, valid, x, y, z, dimId);
			}
		}
		
	}

	public static void sendValidCheckPacket(ItemStack stack, int slotIndex, int x, int y, int z, int dimId) {
		if(!FMLCommonHandler.instance().getSide().isServer()) return;
		
		ByteArrayOutputStream byteStream = new ByteArrayOutputStream();		
		DataOutputStream dataStream = new DataOutputStream(byteStream);
	}

}
