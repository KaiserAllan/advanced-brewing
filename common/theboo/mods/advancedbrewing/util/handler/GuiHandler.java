package theboo.mods.advancedbrewing.util.handler;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.container.ContainerABrewingStand;
import theboo.mods.advancedbrewing.container.ContainerBrewingStation;
import theboo.mods.advancedbrewing.container.ContainerPotionBinder;
import theboo.mods.advancedbrewing.container.ContainerPotionReinforcer;
import theboo.mods.advancedbrewing.gui.GuiABrewingStand;
import theboo.mods.advancedbrewing.gui.GuiBrewingStation;
import theboo.mods.advancedbrewing.gui.GuiPotionBinder;
import theboo.mods.advancedbrewing.gui.GuiPotionReinforcer;
import theboo.mods.advancedbrewing.tileentity.TileEntityABrewingStand;
import theboo.mods.advancedbrewing.tileentity.TileEntityBrewingStation;
import theboo.mods.advancedbrewing.tileentity.TileEntityPotionBinder;
import theboo.mods.advancedbrewing.tileentity.TileEntityPotionReinforcer;
import cpw.mods.fml.common.network.IGuiHandler;

/**
 * AdvancedBrewing GuiHandler
 * 
 * <br> Handles the GUI of the mod.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class GuiHandler implements IGuiHandler {
	
	@Override
	public Object getServerGuiElement(int id, EntityPlayer player, World world,int x, int y, int z) {
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		switch(id) {
		case 0:
			if (tileEntity != null && tileEntity instanceof TileEntityABrewingStand) {
				return new ContainerABrewingStand(player.inventory, (TileEntityABrewingStand) tileEntity);
			}
		case 1:
			if(tileEntity != null && tileEntity instanceof TileEntityBrewingStation) {
				return new ContainerBrewingStation(player.inventory, (TileEntityBrewingStation) tileEntity);
			}
		case 2:
			if(tileEntity != null && tileEntity instanceof TileEntityPotionBinder) {
				return new ContainerPotionBinder(player.inventory, (TileEntityPotionBinder) tileEntity);
			}
		case 3:
			if(tileEntity != null && tileEntity instanceof TileEntityPotionReinforcer) {
				return new ContainerPotionReinforcer(player.inventory, (TileEntityPotionReinforcer) tileEntity);
			}
		break;
		}
		
		return null;
	}

	public Object getClientGuiElement(int id, EntityPlayer player, World world,int x, int y, int z) {		
		TileEntity tileEntity = world.getBlockTileEntity(x, y, z);
		
		switch(id) {
		case 0:
			if (tileEntity != null && tileEntity instanceof TileEntityABrewingStand) {

				return new GuiABrewingStand(player.inventory, (TileEntityABrewingStand) tileEntity);
			}
		case 1:
			if(tileEntity != null && tileEntity instanceof TileEntityBrewingStation) {
				return new GuiBrewingStation(player.inventory, (TileEntityBrewingStation) tileEntity);
			}
		case 2:
			if(tileEntity != null && tileEntity instanceof TileEntityPotionBinder) {
				return new GuiPotionBinder(player.inventory, (TileEntityPotionBinder) tileEntity);
			}
		case 3:
			if(tileEntity != null && tileEntity instanceof TileEntityPotionReinforcer) {
				return new GuiPotionReinforcer(player.inventory, (TileEntityPotionReinforcer) tileEntity);
			}
			break;
		}

		return null;

	}
}