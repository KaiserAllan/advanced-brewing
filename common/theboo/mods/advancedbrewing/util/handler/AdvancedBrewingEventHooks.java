package theboo.mods.advancedbrewing.util.handler;

import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.ChatMessageComponent;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingAttackEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.world.WorldEvent;
import theboo.mods.advancedbrewing.AdvancedBrewing;
import theboo.mods.advancedbrewing.potionapi.PotionRegistry;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.util.ModInfo;
import theboo.mods.util.core.handler.VersionChecker;

/**
 * AdvancedBrewing AdvancedBrewingEventHooks
 * 
 * @author TheBoo
 *
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class AdvancedBrewingEventHooks {

	
	@ForgeSubscribe
	public void onEntityUpdate(LivingUpdateEvent event) {
		if(event.entityLiving.getActivePotionEffect(PotionRegistry.lifeStealer) == null) return;
		if (event.entityLiving.getActivePotionEffect(PotionRegistry.lifeStealer).getDuration()==0) {
			event.entityLiving.removePotionEffect(PotionRegistry.lifeStealer.id);
			return;
		}
	}

	@ForgeSubscribe
	public void onEntityAttacked(LivingAttackEvent event) {
		if(event.source.getEntity() instanceof EntityLivingBase) {
			EntityLivingBase attacker = (EntityLivingBase) event.source.getEntity();

			if (attacker.isPotionActive(PotionRegistry.lifeStealer)) {
				attacker.heal(event.ammount / 16);
				return;
			}	
		}


	}
	
	private boolean checked = false;
	
	@ForgeSubscribe
	public void worldLoad(EntityJoinWorldEvent event) {
		if(!(event.entity instanceof EntityPlayer)) return;
		if(event.world.isRemote) return;
		if(checked) return;
		
		EntityPlayer player = (EntityPlayer) event.entity;
		
		if(VersionChecker.checkIfOutdated(AdvancedBrewing.getUpdateURL(), ModInfo.CURRENT_VERSION_DOUBLE, ModInfo.DEBUG)) {
			if(event.world.provider.dimensionId == 0) {
				player.addChatMessage("Advanced Brewing v" + ModInfo.CURRENT_VERSION_DOUBLE + " is out of date! Please update at https://bitbucket.org/TheBoo/advanced-brewing/wiki/Home" );
				FMLLog.log(Level.WARNING, "Advanced Brewing v" + ModInfo.CURRENT_VERSION_DOUBLE + " is out of date! Please update at https://bitbucket.org/TheBoo/advanced-brewing/wiki/Home");
			}
		} else {
			FMLLog.log(Level.INFO, "Advanced Brewing is up to date.");
		}
		
		//player.addChatMessage(String.valueOf(PotionHelper2.getSize()));
		
		checked = true;
	}
}
