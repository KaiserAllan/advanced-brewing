package theboo.mods.advancedbrewing.configuration;

import java.util.logging.Level;

import cpw.mods.fml.common.FMLLog;
import net.minecraftforge.common.Configuration;
import theboo.mods.advancedbrewing.util.BlockIdReference;
import theboo.mods.advancedbrewing.util.ItemIdReference;

/**
 * AdvancedBrewing Contains all the blocks and items of the mod.
 * @author TheBoo
 *
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
public class ConfigurationLoader {

		
	public void readConfig(Configuration c) {
		try {
			c.load();
			BlockIdReference.ADVANCED_BREWING_STAND = c.getBlock("Advanced Brewing Stand BLOCK ID", BlockIdReference.ADVANCED_BREWING_STAND_DEFAULT).getInt();
			BlockIdReference.BREWING_STATION = c.getBlock("Lab Station BLOCK ID", BlockIdReference.BREWING_STATION_DEFAULT).getInt();
			BlockIdReference.POTION_BINDER = c.getBlock("Potion Binder BLOCK ID", BlockIdReference.POTION_BINDER_DEFAULT).getInt();
			BlockIdReference.POTION_REINFORCER = c.getBlock("Reinforcer BLOCK ID", BlockIdReference.POTION_REINFORCER_DEFAULT).getInt();
			BlockIdReference.CRYSTAL_QUARTZ_ORE = c.getBlock("Crystal Quartz Ore Block ID", BlockIdReference.CRYSTAL_QUARTZ_ORE_DEFAULT).getInt();

			ItemIdReference.ITEM_ADVANCED_BREWING_STAND = c.getItem("Advanced Brewing Stand ITEM ID", ItemIdReference.ITEM_ADVANCED_BREWING_STAND_DEFAULT).getInt();
			ItemIdReference.ITEM_BREWING_STATION = c.getItem("Lab Station ITEM ID", ItemIdReference.ITEM_BREWING_STATION_DEFAULT).getInt();
			ItemIdReference.POTION = c.getItem("Potion ID", ItemIdReference.POTION_DEFAULT).getInt();
			ItemIdReference.SLIME_BINDED = c.getItem("Slime Binder ID", ItemIdReference.SLIME_BINDED_DEFAULT).getInt();
			ItemIdReference.ITEM_POTION_REINFORCER = c.getItem("Reinforcer ITEM ID", ItemIdReference.ITEM_POTION_REINFORCER_DEFAULT).getInt();
			ItemIdReference.CONSUMED_SOUL = c.getItem("Consumed Soul ID", ItemIdReference.CONSUMED_SOUL_DEFAULT).getInt();
			ItemIdReference.MAGICAL_DUST = c.getItem("MagicalDust ID", ItemIdReference.MAGICAL_DUST_DEFAULT).getInt();
			ItemIdReference.CRYSTAL_QUARTZ = c.getItem("Crystal Quartz ID", ItemIdReference.CRYSTAL_QUARTZ_DEFAULT).getInt();
			ItemIdReference.SULFUREO = c.getItem("Sulfurei ID", ItemIdReference.SULFUREI_DEFAULT).getInt();

		}
		catch(Exception e) {
			FMLLog.log(Level.SEVERE, "Advanced Brewing couldn't load its config file.");
		}
		finally {
			c.save();
		}

	}
	

}
