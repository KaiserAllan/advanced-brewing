package theboo.mods.advancedbrewing.potionapi;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.ai.attributes.Attribute;
import net.minecraft.entity.ai.attributes.AttributeModifier;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.EnumAction;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.Icon;
import net.minecraft.util.StatCollector;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.entity.EntityPotion2;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.util.ModInfo;
import theboo.mods.advancedbrewing.util.handler.idsender.IDHolder;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.HashMultimap;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing ItemPotionBase
 * 
 * The base potion class.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class ItemPotionBase extends Item {
    
    private static HashBiMap<Integer, AdvancedPotion> potions = HashBiMap.create(4096);//new HashBiMap<Integer, AdvancedPotion>();
    															//was bimap.create(96) before	
	@SideOnly(Side.CLIENT)
	private Icon drinkableIcon;
	@SideOnly(Side.CLIENT)
	private Icon splashIcon;
	@SideOnly(Side.CLIENT)
	private Icon overlayIcon;
    
	public ItemPotionBase(int par1) {
		super(par1);
        this.setMaxStackSize(1);
        this.setHasSubtypes(true);
        this.setMaxDamage(0);
        this.setCreativeTab(CreativeTabs.tabBrewing);
	}
	
    public int getMaxItemUseDuration(ItemStack par1ItemStack) {
        return 32;
    }

	@SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister) {
    	this.drinkableIcon = ItemPotion.func_94589_d("bottle_drinkable");
    	this.splashIcon = ItemPotion.func_94589_d("bottle_splash");
        this.overlayIcon = ItemPotion.func_94589_d("overlay");
    }
    
	/**
	 * Adds a potion to the metadata of ItemPotionBase by the given ID and AdvancedPotion.
	 * @param id the metadata of the new potion.
	 * @param potion the properties of the potion.
	 */
	public void addPotion(int id, AdvancedPotion potion) {
		potions.put(id, potion);
	}
		
	public int getAvailablePotionID() {
		for(int i=0;i<1096;i++) {
			if(!potions.containsKey(i)) 
				return i;
		}
		
		return 0;
	}
	
	@SideOnly(Side.CLIENT)
	/**
	 * Gets the icon of the potion based on the damage value.
	 */
    public Icon getIconFromDamage(int par1) {
    	return getPotionFromDamage(par1).isSplashPotion() ? splashIcon : drinkableIcon;
    }
	
	public static int getMetadataByEffect(AdvancedPotion potion) {
		BiMap<AdvancedPotion, Integer> bimap = potions.inverse();
		
		if(bimap.containsKey(potion)) {
			return bimap.get(potion);
		}else return 0;
	}
    
    public String getItemDisplayName(ItemStack stack) {
    	StringBuilder name = new StringBuilder();
    	name.append(getPotionFromDamage(stack.getItemDamage()).getName());
    	if(ModInfo.DEBUG) {
    		name.append(" DMG: " + stack);
    	}
    	return name.toString();	
    }
	
    public static AdvancedPotion getPotionFromDamage(int damage) {
		if(!potions.containsKey(damage)) {
			System.out.println("No metadata/key on metadata " + damage + ", returning default potion.");
			return PotionRegistry.useless;		
		}
		return potions.get(damage);
    }
    
	public boolean onItemUse(ItemStack par1ItemStack,EntityPlayer par2EntityPlayer, World par3World, int par4, int par5,
			int par6, int par7, float par8, float par9, float par10) {
		return false;
	}

	
    public ItemStack onEaten(ItemStack par1ItemStack, World par2World, EntityPlayer player)  {
        this.addPotionEffect(par2World, player, par1ItemStack);
        
        getPotionFromDamage(par1ItemStack.getItemDamage()).renew();
        
        if (!player.capabilities.isCreativeMode)
        {
            --par1ItemStack.stackSize;

            if (par1ItemStack.stackSize <= 0)
            {
                return new ItemStack(Item.glassBottle);
            }

            player.inventory.addItemStackToInventory(new ItemStack(Item.glassBottle));
        }
        
        return par1ItemStack;
    }
    
    public EnumAction getItemUseAction(ItemStack par1ItemStack) {
        return EnumAction.drink;
    }
    
    public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (getPotionFromDamage(par1ItemStack.getItemDamage()).isSplashPotion()) {
            if (!par3EntityPlayer.capabilities.isCreativeMode) {
                --par1ItemStack.stackSize;
            } else {
            	ItemStack newstack = par1ItemStack.copy();
                --par1ItemStack.stackSize;
                par3EntityPlayer.inventory.addItemStackToInventory(newstack);
            }

            par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));

            if (!par2World.isRemote) {
            	EntityPotion2 p = new EntityPotion2(par2World, par3EntityPlayer, par1ItemStack);
                par2World.spawnEntityInWorld(p);
                IDHolder.setId(par1ItemStack.getItemDamage());
            }

            return par1ItemStack;
        }
        else {
            par3EntityPlayer.setItemInUse(par1ItemStack, this.getMaxItemUseDuration(par1ItemStack));
            return par1ItemStack;
        }
    }

	protected void addPotionEffect(World world, EntityPlayer player, ItemStack stack) {
		for(int i=0;i<getPotionFromDamage(stack.getItemDamage()).getEffects().length; i++) {
			PotionEffect effect = getPotionFromDamage(stack.getItemDamage()).getEffect(i);
						
	        if (!world.isRemote && effect.getPotionID() > 0) {
	        	player.addPotionEffect(new PotionEffect(effect.getPotionID(), effect.getDuration(), effect.getAmplifier()));
	        }
		}
    }
	
	 public void getSubItems(int par1, CreativeTabs par2CreativeTabs, List par3List) {
		 for(int j=0;j<potions.size();j++) {
			 par3List.add(new ItemStack(par1, 1, j));
		 }
	 }
	 
	 /**
	  * Returns a list of all potion effects.
	  */
	 public List getAllPotionEffects() {
		 List<PotionEffect> list = new ArrayList<PotionEffect>();
		 for(int j=0;j<potions.size();j++) {
			 for(int i=0;i<potions.get(j).getEffects().length;i++) {
				 list.add(potions.get(j).getEffect(i));
			 }
		 }
		 return list;
	 }
	 
	 
	 public void addInformation(ItemStack par1ItemStack, EntityPlayer par2EntityPlayer, List par3List, boolean par4)
	 {
		 if (par1ItemStack.getItemDamage() != 0) {
			 List list1 = PotionHelper2.getPotionEffects(getPotionFromDamage(par1ItemStack.getItemDamage()));
			 HashMultimap hashmultimap = HashMultimap.create();
			 Iterator iterator;

			 if (list1 != null && !list1.isEmpty())
			 {
				 iterator = list1.iterator();

				 while(iterator.hasNext()) {
					 PotionEffect potioneffect = (PotionEffect)iterator.next();
					 String s = StatCollector.translateToLocal(potioneffect.getEffectName()).trim();
					 Potion potion = Potion.potionTypes[potioneffect.getPotionID()];
					 Map map = potion.func_111186_k();

					 if (map != null && map.size() > 0)
					 {
						 Iterator iterator1 = map.entrySet().iterator();

						 while (iterator1.hasNext())
						 {
							 Entry entry = (Entry)iterator1.next();
							 AttributeModifier attributemodifier = (AttributeModifier)entry.getValue();
							 AttributeModifier attributemodifier1 = new AttributeModifier(attributemodifier.getName(), potion.func_111183_a(potioneffect.getAmplifier(), attributemodifier), attributemodifier.getOperation());
							 hashmultimap.put(((Attribute)entry.getKey()).getAttributeUnlocalizedName(), attributemodifier1);
						 }
					 }
					 
					 if (potioneffect.getAmplifier() > 0)
					 {
						 s = s + " " + StatCollector.translateToLocal("potion.potency." + potioneffect.getAmplifier()).trim();
					 }

					 if (potioneffect.getDuration() > 20)
					 {
						 s = s + " (" + Potion.getDurationString(potioneffect) + ")";
					 }

					 if (potion.isBadEffect())
					 {
						 par3List.add(EnumChatFormatting.RED + s);
					 }
					 else
					 {
						 par3List.add(EnumChatFormatting.GRAY + s);
					 }
				 }
				 
			 }
			 else
			 {
				 String s1 = StatCollector.translateToLocal("potion.empty").trim();
				 par3List.add(EnumChatFormatting.GRAY + s1);
			 }

			 
			 if(getPotionFromDamage(par1ItemStack.getItemDamage()).getDisplayInfo()) {
				 par3List.add("");
				 par3List.add(EnumChatFormatting.DARK_PURPLE + StatCollector.translateToLocal("potion.effects.whenDrank"));
				 				 
				 for(int i=0;i<getPotionFromDamage(par1ItemStack.getItemDamage()).getInformation().size();i++) {
					 par3List.add("" + getPotionFromDamage(par1ItemStack.getItemDamage()).getInformation().get(i));
				 }
			 }
		 }
	 }
	 
	  
	 @SideOnly(Side.CLIENT)
	 /**
	  * Calculates the color of the potion based on the damage value.
	  * @param par1 the damage value.
	  * @return the color.
	  */
	 public int getColorFromDamage(int par1) {
		 if(par1 == 0) return 3694022;
		 return PotionHelper2.calcColor(par1, false);
	 }

	 @SideOnly(Side.CLIENT)
	 public int getColorFromItemStack(ItemStack par1ItemStack, int par2) {
		 return par2 > 0 ? 16777215 : this.getColorFromDamage(par1ItemStack.getItemDamage());
	 }

	 @SideOnly(Side.CLIENT)
	 public boolean requiresMultipleRenderPasses()
	 {
		 return true;
	 }

	 public Icon getIconFromDamageForRenderPass(int par1, int par2)
	 {
		 return par2 == 0 ? this.overlayIcon : super.getIconFromDamageForRenderPass(par1, par2);
	 }

}
