package theboo.mods.advancedbrewing.potionapi;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.potion.PotionEffect;
import net.minecraft.util.Icon;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;

/**
 * AdvancedBrewing AdvancedPotion
 * 
 * <br> Used to hold the properties of the potions.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */

public class AdvancedPotion {

	private boolean splash;
	private Icon icon;
	private String name;
	private List<String> list = new ArrayList<String>();
	private PotionEffect[] effects;
	private String[] infoArray;
	private boolean info = false;
	
	private int[] beginDuration;
	
	public AdvancedPotion(PotionEffect... effects) {
		this("Potion of Nullness", effects);
	}
	
	public AdvancedPotion(String name, PotionEffect... effects) {
		this(false, name, effects);
	}
	
	public AdvancedPotion( boolean splash, String name, PotionEffect... effects) {		
		this.splash = splash;
		this.name = name;
		this.effects = effects;
		
		beginDuration = new int[4096];
		
		for(int i = 0;i<effects.length;i++) {
			beginDuration[i] = effects[i].getDuration();
		}
				
		ModItems.potion.addPotion(ModItems.potion.getAvailablePotionID(), this);	
	}
	
	
	public AdvancedPotion(boolean splash, String name, List<PotionEffect> list) {
		this.splash = splash;
		this.name = name;
		
		PotionEffect[] effects = new PotionEffect[4096];
		for(int i=0;i<list.size();i++) {
			effects[i] = list.get(i);
		}
		
		this.effects = effects;
	}
	
	public boolean isSplashPotion() {
		return splash;
	}
	
	public Icon getIcon() {
		return icon==null ? null : icon;
	}
	
	public PotionEffect[] getEffects() {
		return effects;
	}
	
	public PotionEffect getEffect(int i) {
		return effects[i];
	}

	public String getName() {
    	if(isSplashPotion()) {
    		return "Splash " + name;
    	}
		return name;
	}
	
	public String[] getInformationArray() {
		return infoArray;
	}
	
	public List getInformation() {
		return list;
	}
	
	public void addInformation(String... info) {
		if(this.info == false) this.info = true;
		
		this.infoArray = info;
		for(int i=0;i<info.length;i++) {
			list.add(info[i]);
		}
	}
	
	public boolean getDisplayInfo() {
		return info;
	}
	
	public String toString() {
		return "potion: " + name + " effects: " + effects  + " splash potion " + splash;
		
	}
	
	public int[] getBeginDuration() {
		return beginDuration;
	}

	public void setBeginDuration(int[] beginDuration) {
		this.beginDuration = beginDuration;
	}
	
	public void renew() {
		for(int i=0;i<effects.length;i++) {
			effects[i].duration = beginDuration[i];
		}
	}
}
