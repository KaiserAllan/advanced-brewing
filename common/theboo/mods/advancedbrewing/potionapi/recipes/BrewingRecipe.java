package theboo.mods.advancedbrewing.potionapi.recipes;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.potionapi.AdvancedPotion;

/**
 * AdvancedBrewing BrewingRecipe
 * 
 * <br> Registers the brewing recipes.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class BrewingRecipe {

    private ItemStack prevPotion;
    private AdvancedPotion newPotion;
    private int ingredientMeta;
    
    public BrewingRecipe(ItemStack prevPotion, AdvancedPotion newPotion) {
    	this(0, prevPotion, newPotion);
    }
    
    public BrewingRecipe(int ingredientMeta, ItemStack prevPotion, AdvancedPotion newPotion) {
    	this.prevPotion = prevPotion;
    	this.newPotion = newPotion;
    	this.ingredientMeta = ingredientMeta;
    }
    
    public ItemStack getParentPotion() {
    	return prevPotion;
    }
    
    public int getIngredientMeta() {
    	return ingredientMeta;
    }
    
    public AdvancedPotion getBrewingResult() {
    	return newPotion;
    }
}