package theboo.mods.advancedbrewing.potionapi.recipes;

import net.minecraft.item.Item;
import theboo.mods.advancedbrewing.potionapi.AdvancedPotion;

/**
 * AdvancedBrewing BinderRecipe
 * 
 * Contains the properties of potion binding recipes.
 * 
 * @author TheBoo
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
public class BindingRecipe {

	private AdvancedPotion result;
	private Item element;
	private Item binder;

	public BindingRecipe(Item element, Item binder, AdvancedPotion result) {
		this.result = result;
		this.element = element;
		this.binder = binder;
	}

	public AdvancedPotion getResult() {
		return result;
	}

	public void setResult(AdvancedPotion potion1) {
		this.result = potion1;
	}

	public Item getElement() {
		return element;
	}

	public void setElement(Item element) {
		this.element = element;
	}

	public Item getBinder() {
		return binder;
	}

	public void setBinder(Item binder) {
		this.binder = binder;
	}
	
}
