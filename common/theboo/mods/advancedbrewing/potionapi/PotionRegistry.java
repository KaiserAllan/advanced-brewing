package theboo.mods.advancedbrewing.potionapi;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.EnumChatFormatting;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.recipes.BindingRecipe;
import theboo.mods.advancedbrewing.potionapi.recipes.BrewingRecipe;
import theboo.mods.advancedbrewing.potionapi.util.Potion2;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import cpw.mods.fml.common.registry.LanguageRegistry;

/**
 * AdvancedBrewing PotionRegistry
 * 
 * <br> Registers the potions and brewing recipes of the mod.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class PotionRegistry {
	
	public static Potion lifeStealer;
	
	public static AdvancedPotion useless = new AdvancedPotion("Useless Potion", new PotionEffect(Potion.confusion.id, 0, 0));
	
	public static AdvancedPotion blindness = new AdvancedPotion("Potion of Blindness", new PotionEffect(Potion.blindness.id, 3600, 0));
	public static AdvancedPotion blindnessLong = new AdvancedPotion("Potion of Blindness", new PotionEffect(Potion.blindness.id, 9600, 0));
	public static AdvancedPotion blindnessSplash = new AdvancedPotion(true, "Potion of Blindness", new PotionEffect(Potion.blindness.id, 2700, 0));
	public static AdvancedPotion blindnessSplashLong = new AdvancedPotion(true, "Potion of Blindness", new PotionEffect(Potion.blindness.id, 7200, 0));

	public static AdvancedPotion haste = new AdvancedPotion("Potion of Haste", new PotionEffect(Potion.digSpeed.id, 3600, 0));
	public static AdvancedPotion hasteLong = new AdvancedPotion("Potion of Haste", new PotionEffect(Potion.digSpeed.id, 9600, 0));
	public static AdvancedPotion haste2 = new AdvancedPotion("Potion of Haste", new PotionEffect(Potion.digSpeed.id, 1800, 1));
	public static AdvancedPotion hasteSplash = new AdvancedPotion(true, "Potion of Haste", new PotionEffect(Potion.digSpeed.id, 2700, 0));
	public static AdvancedPotion hasteSplashLong = new AdvancedPotion(true, "Potion of Haste", new PotionEffect(Potion.digSpeed.id, 7200, 0));
	public static AdvancedPotion hasteSplash2 = new AdvancedPotion(true, "Potion of Haste", new PotionEffect(Potion.digSpeed.id, hasteSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion haste3 = new AdvancedPotion("Potion of Haste", new PotionEffect(Potion.digSpeed.id, 900, 2));
	public static AdvancedPotion hasteSplash3 = new AdvancedPotion(true, "Potion of Haste", new PotionEffect(Potion.digSpeed.id, 675, 2));

	public static AdvancedPotion mf = new AdvancedPotion("Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 1800, 0));
	public static AdvancedPotion mfLong = new AdvancedPotion("Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 4800, 0));
	public static AdvancedPotion mf2 = new AdvancedPotion("Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 1350, 1));
	public static AdvancedPotion mfSplash = new AdvancedPotion(true, "Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 2700 / 2, 0));
	public static AdvancedPotion mfSplashLong = new AdvancedPotion(true, "Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 3600, 0));
	public static AdvancedPotion mfSplash2 = new AdvancedPotion(true, "Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, mfSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion mf3 = new AdvancedPotion("Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 700, 2));
	public static AdvancedPotion mfSplash3 = new AdvancedPotion(true, "Potion of Dullness", new PotionEffect(Potion.digSlowdown.id, 675, 2));

	public static AdvancedPotion jump = new AdvancedPotion("Potion of Leaping", new PotionEffect(Potion.jump.id, 3600, 0));
	public static AdvancedPotion jumpLong = new AdvancedPotion("Potion of Leaping", new PotionEffect(Potion.jump.id, 9600, 0));
	public static AdvancedPotion jump2 = new AdvancedPotion("Potion of Leaping", new PotionEffect(Potion.jump.id, 1800, 1));
	public static AdvancedPotion jumpSplash = new AdvancedPotion(true, "Potion of Leaping", new PotionEffect(Potion.jump.id, 2700, 0));
	public static AdvancedPotion jumpSplashLong = new AdvancedPotion(true, "Potion of Leaping", new PotionEffect(Potion.jump.id, 7200, 0));
	public static AdvancedPotion jumpSplash2 = new AdvancedPotion(true, "Potion of Leaping", new PotionEffect(Potion.jump.id, hasteSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion jump3 = new AdvancedPotion("Potion of Leaping", new PotionEffect(Potion.jump.id, 900, 2));
	public static AdvancedPotion jumpSplash3 = new AdvancedPotion(true, "Potion of Leaping", new PotionEffect(Potion.jump.id, 675, 2));

	public static AdvancedPotion nausea = new AdvancedPotion("Potion of Nausea", new PotionEffect(Potion.confusion.id, 3600/ 2, 0));
	public static AdvancedPotion nauseaLong = new AdvancedPotion("Potion of Nausea", new PotionEffect(Potion.confusion.id, 9600 / 2, 0));
	public static AdvancedPotion nauseaSplash = new AdvancedPotion(true, "Potion of Nausea", new PotionEffect(Potion.confusion.id, 2700 / 2, 0));
	public static AdvancedPotion nauseaSplashLong = new AdvancedPotion(true, "Potion of Nausea", new PotionEffect(Potion.confusion.id, 7200 / 2, 0));
	
	public static AdvancedPotion resistance = new AdvancedPotion("Potion of Resistance", new PotionEffect(Potion.resistance.id, 3600, 0));
	public static AdvancedPotion resistanceLong = new AdvancedPotion("Potion of Resistance", new PotionEffect(Potion.resistance.id, 9600, 0));
	public static AdvancedPotion resistance2 = new AdvancedPotion("Potion of Resistance", new PotionEffect(Potion.resistance.id, 1800, 1));
	public static AdvancedPotion resistanceSplash = new AdvancedPotion(true, "Potion of Resistance", new PotionEffect(Potion.resistance.id, 2700, 0));
	public static AdvancedPotion resistanceSplashLong = new AdvancedPotion(true, "Potion of Resistance", new PotionEffect(Potion.resistance.id, 7200, 0));
	public static AdvancedPotion resistanceSplash2 = new AdvancedPotion(true, "Potion of Resistance", new PotionEffect(Potion.resistance.id, resistanceSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion resistance3 = new AdvancedPotion("Potion of Resistance", new PotionEffect(Potion.resistance.id, 900, 2));
	public static AdvancedPotion resistanceSplash3 = new AdvancedPotion(true, "Potion of Resistance", new PotionEffect(Potion.resistance.id, 675, 2));

	public static AdvancedPotion wb = new AdvancedPotion("Potion of Water Breathing", new PotionEffect(Potion.waterBreathing.id, 3600, 0));
	public static AdvancedPotion wbLong = new AdvancedPotion("Potion of Water Breathing", new PotionEffect(Potion.waterBreathing.id, 9600, 0));
	public static AdvancedPotion wbSplash = new AdvancedPotion(true, "Potion of Water Breathing", new PotionEffect(Potion.waterBreathing.id, 2700, 0));
	public static AdvancedPotion wbSplashLong = new AdvancedPotion(true, "Potion of Water Breathing", new PotionEffect(Potion.waterBreathing.id, 7200, 0));
	public static AdvancedPotion hunger = new AdvancedPotion("Potion of Hunger", new PotionEffect(Potion.hunger.id, 1800, 0));
	public static AdvancedPotion hungerLong = new AdvancedPotion("Potion of Hunger", new PotionEffect(Potion.hunger.id, 4800, 0));
	public static AdvancedPotion hunger2 = new AdvancedPotion("Potion of Hunger", new PotionEffect(Potion.hunger.id, 1800, 1));
	public static AdvancedPotion hungerSplash = new AdvancedPotion(true, "Potion of Hunger", new PotionEffect(Potion.hunger.id, 2700 / 2, 0));
	public static AdvancedPotion hungerSplashLong = new AdvancedPotion(true, "Potion of Hunger", new PotionEffect(Potion.hunger.id, 3600, 0));
	public static AdvancedPotion hungerSplash2 = new AdvancedPotion(true, "Potion of Hunger", new PotionEffect(Potion.hunger.id, hungerSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion hunger3 = new AdvancedPotion("Potion of Hunger", new PotionEffect(Potion.hunger.id, 900, 2));
	public static AdvancedPotion hungerSplash3 = new AdvancedPotion(true, "Potion of Hunger", new PotionEffect(Potion.hunger.id, 675, 2));

	public static AdvancedPotion decay = new AdvancedPotion("Potion of Decay", new PotionEffect(Potion.wither.id, 900, 0));
	public static AdvancedPotion decayLong = new AdvancedPotion("Potion of Decay", new PotionEffect(Potion.wither.id, 4800 / 2, 0));
	public static AdvancedPotion decay2 = new AdvancedPotion("Potion of Decay", new PotionEffect(Potion.wither.id, 450, 1));
	public static AdvancedPotion decaySplash = new AdvancedPotion(true, "Potion of Decay", new PotionEffect(Potion.wither.id, 2700 / 4, 0));
	public static AdvancedPotion decaySplashLong = new AdvancedPotion(true, "Potion of Decay", new PotionEffect(Potion.wither.id, 1800, 0));
	public static AdvancedPotion decaySplash2 = new AdvancedPotion(true, "Potion of Decay", new PotionEffect(Potion.wither.id, decaySplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion decay3 = new AdvancedPotion("Potion of Decay", new PotionEffect(Potion.wither.id, 225, 2));
	public static AdvancedPotion decaySplash3 = new AdvancedPotion(true, "Potion of Decay", new PotionEffect(Potion.wither.id, 675, 2));
	
	public static AdvancedPotion hb = new AdvancedPotion("Potion of Power", new PotionEffect(Potion.field_76434_w.id, 900, 0));
	public static AdvancedPotion hbLong = new AdvancedPotion("Potion of Power", new PotionEffect(Potion.field_76434_w.id, 4800 / 2, 0));
	public static AdvancedPotion hb2 = new AdvancedPotion("Potion of Power", new PotionEffect(Potion.field_76434_w.id, 900 / 2, 1));
	public static AdvancedPotion hbSplash = new AdvancedPotion(true, "Potion of Power", new PotionEffect(Potion.field_76434_w.id, 2700 / 4, 0));
	public static AdvancedPotion hbSplashLong = new AdvancedPotion(true, "Potion of Power", new PotionEffect(Potion.field_76434_w.id, 1800, 0));
	public static AdvancedPotion hbSplash2 = new AdvancedPotion(true, "Potion of Power", new PotionEffect(Potion.field_76434_w.id, hbSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion hb3 = new AdvancedPotion("Potion of Power", new PotionEffect(Potion.field_76434_w.id, 225, 2));
	public static AdvancedPotion hbSplash3 = new AdvancedPotion(true, "Potion of Power", new PotionEffect(Potion.field_76434_w.id, 675, 2));

	public static AdvancedPotion absorb = new AdvancedPotion("Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, 900, 0));
	public static AdvancedPotion absorbLong = new AdvancedPotion("Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, 4800 / 2, 0));
	public static AdvancedPotion absorb2 = new AdvancedPotion("Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, 900 / 2, 1));
	public static AdvancedPotion absorbSplash = new AdvancedPotion(true, "Potion of Absorbtion", (new PotionEffect(Potion.field_76444_x.id, 2700 / 4, 0)));
	public static AdvancedPotion absorbSplashLong = new AdvancedPotion(true, "Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, 1800, 0));
	public static AdvancedPotion absorbSplash2 = new AdvancedPotion(true, "Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, absorbSplash.getEffect(0).duration / 2, 1));
	public static AdvancedPotion absorb3 = new AdvancedPotion("Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, 225, 2));
	public static AdvancedPotion absorbSplash3 = new AdvancedPotion(true, "Potion of Absorbtion", new PotionEffect(Potion.field_76444_x.id, absorbSplash.getEffect(0).duration / 2, 2));

	public static AdvancedPotion saturation = new AdvancedPotion("Potion of Saturation", new PotionEffect(Potion.field_76443_y.id, 3600, 0));
	public static AdvancedPotion saturationLong = new AdvancedPotion("Potion of Saturation", new PotionEffect(Potion.field_76443_y.id, 9600, 0));
	public static AdvancedPotion saturation2 = new AdvancedPotion("Potion of Saturation", new PotionEffect(Potion.field_76443_y.id, 1800, 1));
	public static AdvancedPotion saturationSplash = new AdvancedPotion(true, "Potion of Saturation", new PotionEffect(Potion.field_76443_y.id, 2700, 0));
	public static AdvancedPotion saturationSplashLong = new AdvancedPotion(true, "Potion of Saturation", new PotionEffect(Potion.field_76443_y.id, 7200, 0));
	public static AdvancedPotion saturationSplash2 = new AdvancedPotion(true, "Potion of Saturation", new PotionEffect(Potion.field_76443_y.id, saturationSplash.getEffect(0).duration / 2, 1));
	
	public static AdvancedPotion hbAndAb = new AdvancedPotion("Potion of Unlimited Power", new PotionEffect(Potion.field_76444_x.id, 1800, 3), new PotionEffect(Potion.field_76434_w.id, 1800, 3));
	public static AdvancedPotion berserker = new AdvancedPotion("Berserker Potion", new PotionEffect(Potion.damageBoost.id, 1800, 4), new PotionEffect(Potion.confusion.id, 1400, 0));
	public static AdvancedPotion strength3 = new AdvancedPotion("Potion of Strength", new PotionEffect(Potion.damageBoost.id, 1800, 2));
	public static AdvancedPotion lastResort = new AdvancedPotion("Last Resort", new PotionEffect(Potion.damageBoost.id, 300, 10), new PotionEffect(Potion.poison.id, 500, 5));
	public static AdvancedPotion potion = new AdvancedPotion("Potion", new PotionEffect(Potion.jump.id, 1000, 2), new PotionEffect(Potion.digSpeed.id, 1000, 2));
	public static AdvancedPotion poison3 = new AdvancedPotion("Potion of Posion", new PotionEffect(Potion.poison.id, 1800, 2));

	
	public static AdvancedPotion lifeStealing;
	public static AdvancedPotion lifeStealingLong;
	public static AdvancedPotion lifeStealing2;
	public static AdvancedPotion lifeStealingSplash;
	public static AdvancedPotion lifeStealingSplashLong;
	public static AdvancedPotion lifeStealingSplash2;

	
	public static void addPotions() {
		addInformation();
		
		PotionHelper2.extendPotionArray();
		
		lifeStealer = new Potion2(30, false, 4738376).setPotionName("Lifestealer");
		
		lifeStealing = new AdvancedPotion("Potion of Lifestealing", new PotionEffect(lifeStealer.id, 900, 0));
		lifeStealingLong = new AdvancedPotion("Potion of Lifestealing", new PotionEffect(lifeStealer.id, 2400, 0));
		lifeStealing2 = new AdvancedPotion(true, "Potion of Lifestealing", new PotionEffect(lifeStealer.id, 900/2, 1));
		lifeStealingSplash = new AdvancedPotion(true, "Potion of Lifestealing", new PotionEffect(lifeStealer.id, 2700 / 4, 0));
		lifeStealingSplashLong = new AdvancedPotion(true, "Potion of Lifestealing", new PotionEffect(lifeStealer.id, 1800, 0));
		lifeStealingSplash2 = new AdvancedPotion(true, "Potion of Lifestealing", new PotionEffect(lifeStealer.id, lifeStealingSplashLong.getEffect(0).duration / 2, 1));
	}

	public static void addPotionRecipes() {
    	PotionHelper2.addBrewing(Item.melonSeeds, new BrewingRecipe(new ItemStack(Item.potion, 1, 0), useless));
        PotionHelper2.addBrewing(Item.dyePowder, new BrewingRecipe(3, new ItemStack(ModItems.potion, 1, 0), haste));
        PotionHelper2.addBrewing(Item.slimeBall, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), mf));
        PotionHelper2.addBrewing(Item.feather, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), jump));
        PotionHelper2.addBrewing(Item.poisonousPotato, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), nausea));
        PotionHelper2.addBrewing(Item.ingotIron, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), resistance));
        PotionHelper2.addBrewing(Item.fishRaw, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), wb));
        PotionHelper2.addBrewing(Item.dyePowder, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), blindness));
        PotionHelper2.addBrewing(Item.rottenFlesh, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), hunger));
        PotionHelper2.addBrewing(Item.coal, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), decay));
        PotionHelper2.addBrewing(Item.appleGold, new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), hb));
		
		addSubPotionBrewing(blindness, blindnessLong, blindnessSplash, blindnessSplashLong);
		addSubPotionBrewing(haste, hasteLong, haste2, haste3, hasteSplash, hasteSplashLong, hasteSplash2, hasteSplash3);
		addSubPotionBrewing(mf, mfLong, mf2, mf3, mfSplash, mfSplashLong, mfSplash2, mfSplash3);
		addSubPotionBrewing(jump, jumpLong, jump2, jump3, jumpSplash, jumpSplashLong, jumpSplash2, jumpSplash3);
		addSubPotionBrewing(nausea, nauseaLong, nauseaSplash, nauseaSplashLong);
		addSubPotionBrewing(resistance, resistanceLong, resistance2, resistance3, resistanceSplash, resistanceSplashLong, resistanceSplash2, resistanceSplash3);
		addSubPotionBrewing(wb, wbLong, wbSplash, wbSplashLong);
		addSubPotionBrewing(hunger, hungerLong, hunger2, hunger3, hungerSplash, hungerSplashLong, hungerSplash2, hungerSplash3);
		addSubPotionBrewing(decay, decayLong, decay2, decay3, decaySplash, decaySplashLong, decaySplash2, decaySplash3);
		addSubPotionBrewing(hb, hbLong, hb2, hb3, hbSplash, hbSplashLong, hbSplash2, hbSplash3);
		addSubPotionBrewing(absorb, absorbLong, absorb2, absorb3, absorbSplash, absorbSplashLong, absorbSplash2, absorbSplash3);
		

		PotionHelper2.addBindingRecipe(strength3, nausea, new BindingRecipe(Item.blazePowder, Item.slimeBall, berserker));
		PotionHelper2.addBindingRecipe(poison3, berserker, Item.blazeRod, ModItems.slimeBinded, lastResort);
		PotionHelper2.addBindingRecipe(haste2, jump, Item.blazeRod, ModItems.slimeBinded, potion);

		//
		
		
		//PotionHelper2.addBindingRecipe(new BinderRecipe(hb, absorb, Item.coal, Item.slimeBall, hbAndAb));
	}
	
	private static void addInformation() {
		useless.addInformation("No effect");		
		blindness.addInformation(EnumChatFormatting.RED + "- Decreased Sight" , EnumChatFormatting.RED + "- Disabled Sprinting");	
		blindnessLong.addInformation(blindness.getInformationArray());
		blindnessSplash.addInformation(blindness.getInformationArray());
		blindnessSplashLong.addInformation(blindness.getInformationArray());
		haste.addInformation(EnumChatFormatting.YELLOW + "+20% Block Breaking Speed");
		hasteLong.addInformation(haste.getInformationArray());
		haste2.addInformation(EnumChatFormatting.YELLOW + "+40% Block Breaking Speed");
		hasteSplash.addInformation(haste.getInformationArray());
		hasteSplashLong.addInformation(haste.getInformationArray());
		hasteSplash2.addInformation(haste2.getInformationArray());
		mf.addInformation(EnumChatFormatting.RED + "-20% Block Breaking Speed");
		mfLong.addInformation(mf.getInformationArray());
		mf2.addInformation(EnumChatFormatting.RED + "-40% Block Breaking Speed");
		mfSplash.addInformation(mf.getInformationArray());
		mfSplashLong.addInformation(mf.getInformationArray());
		mfSplash2.addInformation(mf2.getInformationArray());
		jump.addInformation(EnumChatFormatting.AQUA + "+50% Jumping Height" + "" + "Fall damage reduced by 1.");
		jumpLong.addInformation(jump.getInformationArray());
		jump2.addInformation(EnumChatFormatting.AQUA + "+50.71% Jumping Height" + "" + "Fall damage reduced by 2.");
		jumpSplash.addInformation(jump.getInformationArray());
		jumpSplashLong.addInformation(jump.getInformationArray());
		jumpSplash2.addInformation(jump2.getInformationArray());
		resistance.addInformation(EnumChatFormatting.DARK_GRAY + "Reduces all incoming damage by 20%");
		resistanceLong.addInformation(resistance.getInformationArray());
		resistance2.addInformation(EnumChatFormatting.AQUA + "Reduces all incoming damage by 40%");
		resistanceSplash.addInformation(resistance.getInformationArray());
		resistanceSplashLong.addInformation(resistance.getInformationArray());
		resistanceSplash2.addInformation(resistance2.getInformationArray());	
		
		LanguageRegistry.instance().addStringLocalization("potion.potency.4", "IV");
		LanguageRegistry.instance().addStringLocalization("potion.potency.5", "V");
		LanguageRegistry.instance().addStringLocalization("potion.potency.6", "VI");
		LanguageRegistry.instance().addStringLocalization("potion.potency.7", "VII");
		LanguageRegistry.instance().addStringLocalization("potion.potency.8", "IIX");
		LanguageRegistry.instance().addStringLocalization("potion.potency.9", "IX");
		LanguageRegistry.instance().addStringLocalization("potion.potency.10", "X");

		
		
	}
	
	public static void addSubPotionBrewing(AdvancedPotion parent, AdvancedPotion longer, AdvancedPotion increased, AdvancedPotion increased2, 
			AdvancedPotion splash, AdvancedPotion splashLonger, AdvancedPotion splashIncreased, AdvancedPotion splashIncreased2) {
		PotionHelper2.addBrewing(ModItems.crystalQuartz, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(parent)), longer));
		PotionHelper2.addBrewing(ModItems.magicalDust, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(parent)), increased));
		PotionHelper2.addBrewing(ModItems.magicalDust, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(increased)), increased2));
		
		PotionHelper2.addBrewing(ModItems.sulfureo, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(parent)), splash));
		PotionHelper2.addBrewing(ModItems.crystalQuartz, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(splash)), splashLonger));
		PotionHelper2.addBrewing(ModItems.magicalDust, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(splash)), splashIncreased));
		PotionHelper2.addBrewing(ModItems.magicalDust, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(splashIncreased)), splashIncreased2));
	}
	
	public static void addSubPotionBrewing(AdvancedPotion parent, AdvancedPotion longer, AdvancedPotion splash, AdvancedPotion splashLonger) {
		PotionHelper2.addBrewing(ModItems.crystalQuartz, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(parent)), longer));		
		PotionHelper2.addBrewing(ModItems.sulfureo, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(parent)), splash));
		PotionHelper2.addBrewing(ModItems.crystalQuartz, new BrewingRecipe(new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(splash)), splashLonger));
	}


}
