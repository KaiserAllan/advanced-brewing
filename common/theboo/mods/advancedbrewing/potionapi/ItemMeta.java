package theboo.mods.advancedbrewing.potionapi;

import net.minecraft.item.Item;

public class ItemMeta {

	private int meta;
	private Item item;

	public ItemMeta(Item item, int meta)  {
		this.item = item;
		this.meta = meta;
	}

	public int getMeta() {
		return meta;
	}

	public void setMeta(int meta) {
		this.meta = meta;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}
}
