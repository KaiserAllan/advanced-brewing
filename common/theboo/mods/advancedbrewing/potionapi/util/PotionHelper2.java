package theboo.mods.advancedbrewing.potionapi.util;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.potion.PotionHelper;
import theboo.mods.advancedbrewing.AdvancedBrewing;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.AdvancedPotion;
import theboo.mods.advancedbrewing.potionapi.ItemPotionBase;
import theboo.mods.advancedbrewing.potionapi.PotionRegistry;
import theboo.mods.advancedbrewing.potionapi.recipes.BindingRecipe;
import theboo.mods.advancedbrewing.potionapi.recipes.BrewingRecipe;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing PotionHelper2
 * 
 * <br> Used as a utility class to help with potions.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class PotionHelper2 {

	private static Table<Integer, Item, BrewingRecipe> brewing = HashBasedTable.create();
	private static Table<AdvancedPotion, AdvancedPotion, BindingRecipe> bindings = HashBasedTable.create();//	private static List<BinderRecipe> bindings = new ArrayList<BinderRecipe>();
	private static HashMap<Integer, BindingRecipe> recipes = new HashMap<Integer, BindingRecipe>();
											   						
	private static final int MAX_SIZE = 999999;
	
	
	public static final int MASTER_POTION = 9000;
	public static final int PARENT_POTION = 0;
	public static final int LONGER_POTION = 1;
	public static final int INCREASED_EFFECT_POTION = 2;
	public static final int INCREASED_EFFECT_POTION_2 = 3;
	public static final int SPLASH_POTION = 4;
	public static final int UNBREWABLE = 9000;
		
	public static final int NONE = 0;
	public static final int SUB_PARENT_POTION = 1;
	public static final int SUB_LONGER_POTION = 2;
	public static final int SUB_INCREASED_EFFECT_POTION = 3;
	public static final int SUB_INCREASED_EFFECT_POTION_2 = 4;
	
	public static void addBindingRecipe(AdvancedPotion p1, AdvancedPotion p2, Item element, Item binder, AdvancedPotion result) {
		addBindingRecipe(p1, p2, new BindingRecipe(element, binder, result));
	}
	
	public static void addBindingRecipe(AdvancedPotion p1, AdvancedPotion p2, BindingRecipe recipe) {
		bindings.put(p1, p2, recipe);
		
		for(int i=0;i<4096;i++){
			if(!recipes.containsKey(i)) {
				recipes.put(i, recipe);
				break;
			} 
		}
		
	}
	
	/*public static void addVanillaBrewing() {
		Item[] items = Item.itemsList;
		for(int i=0;i<items.length;i++) {
			addBrewing(items[i], new BrewingRecipe(new ItemStack(Item.potion, 1, 0), PotionRegistry.useless));
			addBrewing(items[i], new BrewingRecipe(new ItemStack(ModItems.potion, 1, 0), PotionRegistry.useless));
		}
	}*/
	
	public static boolean getCanPotionsBind(AdvancedPotion p1, AdvancedPotion p2, Item element, Item binder) {
		if(bindings.contains(p1, p2)) {
			if(bindings.get(p1, p2).getBinder() == binder && bindings.get(p1, p2).getElement() == element)
				return true;
		}
		else {
			if(!Potion.potionTypes[p1.getEffect(0).getPotionID()].isBadEffect() && Potion.potionTypes[p2.getEffect(0).getPotionID()].isBadEffect()) {
				if(element == Item.blazeRod && binder == ModItems.slimeBinded) {
					return true;
				}				
			}
		}
		
		return false;
	
	}
	
	public static boolean isElement(Item element) {
		for(int i=0;i<4096;i++) {
			if(recipes.containsKey(i)) {
				if(recipes.get(i).getElement() == element)
					return true;
			}
		} 

		return false;
	}
	
	public static boolean isBinder(Item binder) {		
		for(int i=0;i<4096;i++) {
			if(recipes.containsKey(i)) {
				if(recipes.get(i).getBinder() == binder)
					return true;
			}
		}

		return false;
	}
	
	public static AdvancedPotion getResultFromBinding(AdvancedPotion p1, AdvancedPotion p2, Item element, Item binder) {
		if(getCanPotionsBind(p1, p2, element, binder)) {		
			if(bindings.contains(p1, p2)) return bindings.get(p1, p2).getResult();
			else return combine(p1, p2);
		}
				
		return PotionRegistry.useless;
	}
	
	public static AdvancedPotion combine(AdvancedPotion p1, AdvancedPotion p2) {
		PotionEffect e1 = p1.getEffect(0);
		PotionEffect e2 = p2.getEffect(0);
		PotionEffect ne1 = new PotionEffect(e1.getPotionID(), e1.duration, increase(e1.getAmplifier()));
		PotionEffect ne2 = new PotionEffect(e2.getPotionID(), e2.duration, increase(e2.getAmplifier()));
						
		return new AdvancedPotion(false, "Potion of " +  e1.getEffectName().substring(7) + " and " + e2.getEffectName().substring(7), ne1, ne2);
	}
	
	public static AdvancedPotion convert(ItemStack stack) {
		if(!(stack.getItem() instanceof ItemPotion)) return logAndReturn();
		ItemPotion potion = (ItemPotion) stack.getItem();
		List<PotionEffect> effects = potion.getEffects(stack);	
		
		return new AdvancedPotion(false, potion.getItemDisplayName(stack), effects);	
	}
	
	public static AdvancedPotion logAndReturn() {
		AdvancedBrewing.logAndPrint(Level.WARNING, "FAILED BREWING ");
		return PotionRegistry.useless;
	}
	
	public static int increase(int x) {
		if(x == 0) return 1;
		else return x * 2;
	}

	public static void addBrewing(Item ingredient, BrewingRecipe recipe) {
		int z = 0;
		
		for(int i=0;i<MAX_SIZE;i++){
			if(!brewing.contains(i, ingredient)) {
				brewing.put(i, ingredient, recipe);
				return;
			}
			z = i;
		}
		
		System.out.println("Cannot add " + ingredient.getUnlocalizedNameInefficiently(new ItemStack(ingredient)) + " to brewing list: too many indexes! Max index: " + z);
	}
	
	public static boolean getCanBrew(int ingDmg, Item ingredient, ItemStack parent) {
		for(int i=0;i<MAX_SIZE;i++) {
			if(!(isIngredient(ingDmg, ingredient))) continue;
			int id = parent.itemID;
			int m = parent.getItemDamage();
			int s = parent.stackSize;
			
			if(!brewing.contains(i, ingredient)) continue;
			
			if(brewing.get(i, ingredient).getParentPotion().itemID == id && brewing.get(i, ingredient).getParentPotion().getItemDamage() == m && 
					brewing.get(i, ingredient).getParentPotion().stackSize == s) {
				return true;
			}		
		}
		
		return false;
	}
	
	public static boolean isIngredient(int ingDmg, Item item) {
		if(!brewing.containsColumn(item)) return false;
		for(int i=0;i<MAX_SIZE ;i++) {
			if(brewing.get(i, item).getIngredientMeta() == ingDmg && brewing.containsColumn(item)) return true;
		}
		return false;
	}
	
	public static AdvancedPotion getBrewing(int ingDmg, Item ingredient, ItemStack parent) {
		if(ingredient != null && brewing.containsColumn(ingredient) && getCanBrew(ingDmg, ingredient, parent)) {
			for(int i=0;i<MAX_SIZE;i++){
				if(brewing.contains(i, ingredient)) {
					int id = parent.itemID;
					int m = parent.getItemDamage();
					int s = parent.stackSize;
					
					if(brewing.get(i, ingredient).getParentPotion().itemID == id && brewing.get(i, ingredient).getParentPotion().getItemDamage() == m && 
							brewing.get(i, ingredient).getParentPotion().stackSize == s) {
						return brewing.get(i, ingredient).getBrewingResult();
					}
				}
			}
		}
		
		return PotionRegistry.useless;
	}
	
	public static List<PotionEffect> getPotionEffects(AdvancedPotion ap) {
		 ArrayList<PotionEffect> arraylist = null;
		 Potion[] apotion = Potion.potionTypes;
		 int j = apotion.length;

		 for (int k = 0; k < j; k++){
			 Potion potion = apotion[k];
	            
			 if (potion != null) {
				 
				 if (arraylist == null) {
					 arraylist = new ArrayList<PotionEffect>();
				 }

				 PotionEffect[] effects = ap.getEffects();
				 
				 for(int i=0;i<effects.length;i++) {
					 PotionEffect potioneffect = ap.getEffect(i);
				 
					 if (ap.isSplashPotion())
					 {
						 potioneffect.setSplashPotion(true);
					 }

					 if(!arraylist.contains(potioneffect)) 
						 arraylist.add(potioneffect);
				 }
			 }
		 }
	        return arraylist;
	}
	
	private static HashMap<Integer, Integer> colors = new HashMap<Integer, Integer>();
	
	 @SideOnly(Side.CLIENT)
	 public static int calcColor(int par0, boolean par1)
	 {
		 if (!par1)	 {
			 if (colors.containsKey(Integer.valueOf(par0))) {
				 return ((Integer)colors.get(Integer.valueOf(par0))).intValue();
			 }
			 else {
				 int j = PotionHelper.calcPotionLiquidColor(PotionHelper2.getPotionEffects(ItemPotionBase.getPotionFromDamage(par0)));
				 colors.put(Integer.valueOf(par0), Integer.valueOf(j));
				 return j;
			 }
		 }
		 else {
			 return PotionHelper.calcPotionLiquidColor(PotionHelper2.getPotionEffects(ItemPotionBase.getPotionFromDamage(par0)));
		 }
	 }
	 
	
	public static int calcColorUsingDefaults(AdvancedPotion ap) {
		int effect = ap.getEffect(0).getPotionID();
	
		if(effect == Potion.blindness.id) {
			
		}
		
		return 0;
	}
	
	public static void extendPotionArray() {
		Potion[] potionTypes = null;

		for (Field f : Potion.class.getDeclaredFields()) {
			f.setAccessible(true);
			try {
				if (f.getName().equals("potionTypes") || f.getName().equals("field_76425_a")) {
					Field modfield = Field.class.getDeclaredField("modifiers");
					modfield.setAccessible(true);
					modfield.setInt(f, f.getModifiers() & ~Modifier.FINAL);

					potionTypes = (Potion[])f.get(null);
					final Potion[] newPotionTypes = new Potion[256];
					System.arraycopy(potionTypes, 0, newPotionTypes, 0, potionTypes.length);
					f.set(null, newPotionTypes);
				}
			}
			catch (Exception e) {
				System.err.println("Severe error occured:");
				System.err.println(e);
			}
		}
	}
}