package theboo.mods.advancedbrewing.tileentity;

import net.minecraft.tileentity.TileEntity;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModTileEntities {

	public static void init() {
		GameRegistry.registerTileEntity((Class<? extends TileEntity>)TileEntityABrewingStand.class, "aBrewingStand");
		GameRegistry.registerTileEntity((Class<? extends TileEntity>)TileEntityBrewingStation.class, "labStation");
		GameRegistry.registerTileEntity((Class<? extends TileEntity>)TileEntityPotionBinder.class, "binder");
		GameRegistry.registerTileEntity((Class<? extends TileEntity>)TileEntityPotionReinforcer.class, "reinforcer");
	}
}
