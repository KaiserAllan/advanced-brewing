package theboo.mods.advancedbrewing.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.AdvancedPotion;
import theboo.mods.advancedbrewing.potionapi.ItemPotionBase;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing TileEntityPotionBinder
 * 
 * <br> Tile entity class for the potion binder.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */

public class TileEntityPotionBinder extends TileEntity implements ISidedInventory
{
    private static final int[] ingredientSlot = new int[] {3, 4};
    private static final int[] potionSlots = new int[] {0, 1, 2};

    private ItemStack[] brewingItemStacks = new ItemStack[5];
    private int brewTime;

    private int filledSlots;
    private int ingredientID;
    private int binderID;
    private String field_94132_e;

    public String getInvName()
    {
        return this.isInvNameLocalized() ? this.field_94132_e : "Potion Binder";
    }

    public boolean isInvNameLocalized()
    {
        return this.field_94132_e != null && this.field_94132_e.length() > 0;
    }

    public void func_94131_a(String par1Str)
    {
        this.field_94132_e = par1Str;
    }

    public int getSizeInventory()
    {
        return this.brewingItemStacks.length;
    }

    public void updateEntity()
    {
        if (this.brewTime > 0)
        {
            --this.brewTime;

            if (this.brewTime == 0)
            {
                this.brewPotions();
                this.onInventoryChanged();
            }
            else if (!this.canBrew())
            {
                this.brewTime = 0;
                this.onInventoryChanged();
            }
            else if (this.ingredientID != this.brewingItemStacks[3].itemID)
            {
                this.brewTime = 0;
                this.onInventoryChanged();
            }
            else if (this.binderID != this.brewingItemStacks[4].itemID)
            {
                this.brewTime = 0;
                this.onInventoryChanged();
            }
        }
        else if (this.canBrew())
        {
            this.brewTime = 400;
            this.ingredientID = this.brewingItemStacks[3].itemID;
            this.binderID = this.brewingItemStacks[4].itemID;
        }

        int i = this.getFilledSlots();

        if (i != this.filledSlots)
        {
            this.filledSlots = i;
            this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, i, 2);
        }

        super.updateEntity();
    }

    public int getBrewTime()
    {
        return this.brewTime;
    }

    private boolean canBrew()
    {
        if (this.brewingItemStacks[3] != null && this.brewingItemStacks[3].stackSize > 0 && this.brewingItemStacks[4] != null && this.brewingItemStacks[2] != null && this.brewingItemStacks[2].getItem() == ModItems.potion && this.brewingItemStacks[2].getItemDamage() == 0)
        {
        	for(int i=0;i<2;i++) {
        		if(this.brewingItemStacks[i] == null) return false;
        	}
        	
        	ItemStack potion1 = this.brewingItemStacks[0];
        	ItemStack potion2 = this.brewingItemStacks[1];
        	
        	AdvancedPotion p1 = null;
        	AdvancedPotion p2 = null;
        	
            if(potion1.getItem() == Item.potion) {
                p1 = PotionHelper2.convert(potion1);
            } else if(potion1.getItem() == ModItems.potion) {
            	 p1 = ItemPotionBase.getPotionFromDamage(brewingItemStacks[0].getItemDamage());
            }
            if(potion1.getItem() == Item.potion) {
                p2 = PotionHelper2.convert(potion2);
            } else if(potion2.getItem() == ModItems.potion) {
            	p2 = ItemPotionBase.getPotionFromDamage(brewingItemStacks[1].getItemDamage());
            }
            
    		Item element = brewingItemStacks[3].getItem();
    		Item binder = brewingItemStacks[4].getItem();
    	
    		if(PotionHelper2.getCanPotionsBind(p1, p2, element, binder))
    			return true;
        }
        
		return false;

    }

	private void brewPotions()
    {
        if (this.canBrew())
        {            
        	ItemStack potion1 = this.brewingItemStacks[0];
        	ItemStack potion2 = this.brewingItemStacks[1];
            ItemStack result = this.brewingItemStacks[2];
            ItemStack itemstack = this.brewingItemStacks[3];
            ItemStack binderstack = this.brewingItemStacks[4];

            
        	AdvancedPotion p1 = null;
        	AdvancedPotion p2 = null;
        	
            if(potion1.getItem() == Item.potion) {
                p1 = PotionHelper2.convert(potion1);
            } else if(potion1.getItem() == ModItems.potion) {
            	p1 = ItemPotionBase.getPotionFromDamage(brewingItemStacks[0].getItemDamage());
            }
            if(potion1.getItem() == Item.potion) {
                p2 = PotionHelper2.convert(potion2);
            } else if(potion2.getItem() == ModItems.potion) {
            	p2 = ItemPotionBase.getPotionFromDamage(brewingItemStacks[1].getItemDamage());
            }
            
            if(p1 == null && p2 == null) return;
        	
        	Item element = itemstack.getItem();
        	Item binder = binderstack.getItem();
            
        	this.setInventorySlotContents(2, new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(PotionHelper2.getResultFromBinding(p1, p2, element, binder))));
            
            if (Item.itemsList[itemstack.itemID].hasContainerItem()) {
                this.brewingItemStacks[3] = Item.itemsList[itemstack.itemID].getContainerItemStack(brewingItemStacks[3]);
                this.brewingItemStacks[4] = Item.itemsList[itemstack.itemID].getContainerItemStack(brewingItemStacks[4]);
            }
            else
            {
                --this.brewingItemStacks[3].stackSize;

                if (this.brewingItemStacks[3].stackSize <= 0)
                {
                    this.brewingItemStacks[3] = null;
                }
                
                --this.brewingItemStacks[0].stackSize;

                if (this.brewingItemStacks[0].stackSize <= 0)
                {
                    this.brewingItemStacks[0] = null;
                }
                
                --this.brewingItemStacks[1].stackSize;

                if (this.brewingItemStacks[1].stackSize <= 0)
                {
                    this.brewingItemStacks[1] = null;
                }
                
                --this.brewingItemStacks[4].stackSize;

                if (this.brewingItemStacks[4].stackSize <= 0)
                {
                    this.brewingItemStacks[4] = null;
                }
            }
        }
    }

    public void readFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.readFromNBT(par1NBTTagCompound);
        NBTTagList nbttaglist = par1NBTTagCompound.getTagList("Items");
        this.brewingItemStacks = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.brewingItemStacks.length)
            {
                this.brewingItemStacks[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }

        this.brewTime = par1NBTTagCompound.getShort("BrewTime");

        if (par1NBTTagCompound.hasKey("CustomName"))
        {
            this.field_94132_e = par1NBTTagCompound.getString("CustomName");
        }
    }

    public void writeToNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.writeToNBT(par1NBTTagCompound);
        par1NBTTagCompound.setShort("BrewTime", (short)this.brewTime);
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.brewingItemStacks.length; ++i)
        {
            if (this.brewingItemStacks[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                this.brewingItemStacks[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        par1NBTTagCompound.setTag("Items", nbttaglist);

        if (this.isInvNameLocalized())
        {
            par1NBTTagCompound.setString("CustomName", this.field_94132_e);
        }
    }

    public ItemStack getStackInSlot(int par1)
    {
        return par1 >= 0 && par1 < this.brewingItemStacks.length ? this.brewingItemStacks[par1] : null;
    }

    public ItemStack decrStackSize(int par1, int par2)
    {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length)
        {
            ItemStack itemstack = this.brewingItemStacks[par1];
            this.brewingItemStacks[par1] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    public ItemStack getStackInSlotOnClosing(int par1)
    {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length)
        {
            ItemStack itemstack = this.brewingItemStacks[par1];
            this.brewingItemStacks[par1] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    public void setInventorySlotContents(int par1, ItemStack par2ItemStack)
    {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length)
        {
            this.brewingItemStacks[par1] = par2ItemStack;
        }
    }

    public int getInventoryStackLimit()
    {
        return 64;
    }

    public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
    {
        return this.worldObj.getBlockTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : par1EntityPlayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }

    public void openChest() {}

    public void closeChest() {}

    public boolean isItemValidForSlot(int par1, ItemStack par2ItemStack)
    {
    	if(par2ItemStack.getItem() instanceof ItemPotion || par2ItemStack.itemID == Item.glassBottle.itemID) 
    		return true;
    	else if(par2ItemStack.getItem() instanceof ItemPotionBase) 
    		return true;
    	else if(PotionHelper2.isElement(par2ItemStack.getItem()))
    		return true;
    	else if(PotionHelper2.isBinder(par2ItemStack.getItem()))
    		return true;
     	else return false;
    }

    @SideOnly(Side.CLIENT)
    public void setBrewTime(int par1)
    {
        this.brewTime = par1;
    }

    public int getFilledSlots()
    {
        int i = 0;

        for (int j = 0; j < 4; ++j)
        {
            if (this.brewingItemStacks[j] != null)
            {
                i |= 1 << j;
            }
        }

        return i;
    }

    public int[] getAccessibleSlotsFromSide(int par1)
    {
        return par1 == 1 ? ingredientSlot : potionSlots;
    }

    public boolean canInsertItem(int par1, ItemStack par2ItemStack, int par3)
    {
        return this.isItemValidForSlot(par1, par2ItemStack);
    }

    public boolean canExtractItem(int par1, ItemStack par2ItemStack, int par3)
    {
        return true;
    }
}
