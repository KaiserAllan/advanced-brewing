package theboo.mods.advancedbrewing.tileentity;

import java.util.List;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.potion.PotionHelper;
import net.minecraft.tileentity.TileEntity;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.AdvancedPotion;
import theboo.mods.advancedbrewing.potionapi.ItemPotionBase;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing TileEntityPotionReinforcer
 * 
 * <br> Tile entity class for the potion reinforcer.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */

public class TileEntityPotionReinforcer extends TileEntityElectrized
{
    public TileEntityPotionReinforcer() {
		super(3);
	}

	private static final int[] ingredientSlot = new int[] {1, 2};
    private static final int[] potionSlots = new int[] {0};

    private int brewTime;

    private int filledSlots;
    private int ingredientID;
    private int elementID;
    private String field_94132_e;

    public String getInvName()
    {
        return this.isInvNameLocalized() ? this.field_94132_e : "Potion Reinforcer";
    }

    public boolean isInvNameLocalized()
    {
        return this.field_94132_e != null && this.field_94132_e.length() > 0;
    }

    public void func_94131_a(String par1Str)
    {
        this.field_94132_e = par1Str;
    }

    public int getSizeInventory()
    {
        return this.brewingItemStacks.length;
    }

    public void updateEntity()
    {
        if (this.brewTime > 0)
        {
            --this.brewTime;

            if (this.brewTime == 0)
            {
                this.brewPotions();
                this.onInventoryChanged();
            }
            else if (!this.canBrew())
            {
                this.brewTime = 0;
                this.onInventoryChanged();
            }
            else if (this.ingredientID != this.brewingItemStacks[1].itemID)
            {
                this.brewTime = 0;
                this.onInventoryChanged();
            }
            else if (this.elementID != this.brewingItemStacks[2].itemID)
            {
                this.brewTime = 0;
                this.onInventoryChanged();
            }
        }
        else if (this.canBrew())
        {
            this.brewTime = 400;
            this.ingredientID = this.brewingItemStacks[1].itemID;
            this.elementID = this.brewingItemStacks[2].itemID;
        }

        int i = this.getFilledSlots();

        if (i != this.filledSlots)
        {
            this.filledSlots = i;
            this.worldObj.setBlockMetadataWithNotify(this.xCoord, this.yCoord, this.zCoord, i, 2);
        }

        super.updateEntity();
    }

    public int getBrewTime()
    {
        return this.brewTime;
    }

    private boolean canBrew()
    {
        if (this.brewingItemStacks[1] != null && this.brewingItemStacks[1].stackSize > 0 && this.brewingItemStacks[2] != null && this.brewingItemStacks[2].getItem() == ModItems.consumedSoul)
        {
            ItemStack itemstack = this.brewingItemStacks[1];
            Item item = itemstack.getItem();

            if (!Item.itemsList[itemstack.itemID].isPotionIngredient() && !(PotionHelper2.isIngredient(itemstack.getItemDamage(), itemstack.getItem())))
            {
                return false;
            }
            else
            {
                boolean flag = false;
                
                    if (this.brewingItemStacks[0] != null && this.brewingItemStacks[0].getItem() instanceof ItemPotion)
                    {
                        int j = this.brewingItemStacks[0].getItemDamage();
                        int k = this.getPotionResult(j, itemstack);

                        if (!ItemPotion.isSplash(j) && ItemPotion.isSplash(k))
                        {
                            flag = true;
                        }

                        List list = Item.potion.getEffects(j);
                        List list1 = Item.potion.getEffects(k);

                        if ((j <= 0 || list != list1) && (list == null || !list.equals(list1) && list1 != null) && j != k)
                        {
                            flag = true;
                        }
                        
                        if(PotionHelper2.getCanBrew(itemstack.getItemDamage(), itemstack.getItem(), brewingItemStacks[0])) {
                        	flag = true;   
                        }
                        
                    }
                    else if(this.brewingItemStacks[0] != null && this.brewingItemStacks[0].getItem() instanceof ItemPotionBase) {
                        if(brewingItemStacks[0].getItem() == ModItems.potion) {
                        	if(PotionHelper2.getCanBrew(itemstack.getItemDamage(), itemstack.getItem(), brewingItemStacks[0])) 
                        		flag = true;
                        }
                    }
                return flag;
            }
        }
        else
        {
            return false;
        }
    }

	private void brewPotions()
    {
        if (this.canBrew())
        {
            ItemStack itemstack = this.brewingItemStacks[1];
            Item item = itemstack.getItem();
            
                if (this.brewingItemStacks[0] != null && this.brewingItemStacks[0].getItem() instanceof ItemPotion)
                {
                    int j = this.brewingItemStacks[0].getItemDamage();
                    int k = this.getPotionResult(j, itemstack);
                    List list = Item.potion.getEffects(j);
                    List list1 = Item.potion.getEffects(k);

                    if ((j <= 0 || list != list1) && (list == null || !list.equals(list1) && list1 != null))
                    {
                        if (j != k)
                        {
                            this.brewingItemStacks[0].setItemDamage(k);
                        }
                    }
                    else if (!ItemPotion.isSplash(j) && ItemPotion.isSplash(k))
                    {
                        this.brewingItemStacks[0].setItemDamage(k);
                    }
             
                    if(brewingItemStacks[0].getItem() == Item.potion && brewingItemStacks[0].getItemDamage() == 0) {
                    	AdvancedPotion p = PotionHelper2.getBrewing(itemstack.getItemDamage(), itemstack.getItem(), brewingItemStacks[0]);
                    	setInventorySlotContents(0, new ItemStack(ModItems.potion, 1, ItemPotionBase.getMetadataByEffect(p)));
                    }
                }
            
                else if(this.brewingItemStacks[0] != null) {
                    if(brewingItemStacks[0].getItem() == ModItems.potion) {
                    	AdvancedPotion p = PotionHelper2.getBrewing(itemstack.getItemDamage(), itemstack.getItem(), brewingItemStacks[0]);
                    	brewingItemStacks[0].setItemDamage(ItemPotionBase.getMetadataByEffect(p));
                    }
                }
            if (Item.itemsList[itemstack.itemID].hasContainerItem())
            {
                this.brewingItemStacks[1] = Item.itemsList[itemstack.itemID].getContainerItemStack(brewingItemStacks[1]);
                this.brewingItemStacks[2] = Item.itemsList[itemstack.itemID].getContainerItemStack(brewingItemStacks[2]);
            }
            else
            {
                --this.brewingItemStacks[1].stackSize;

                if (this.brewingItemStacks[1].stackSize <= 0)
                {
                    this.brewingItemStacks[1] = null;
                }
                
                --this.brewingItemStacks[2].stackSize;

                if (this.brewingItemStacks[2].stackSize <= 0)
                {
                    this.brewingItemStacks[2] = null;
                }
            }
        }
    }

    private int getPotionResult(int par1, ItemStack par2ItemStack)
    {
        return par2ItemStack == null ? par1 : (Item.itemsList[par2ItemStack.itemID].isPotionIngredient() ? PotionHelper.applyIngredient(par1, Item.itemsList[par2ItemStack.itemID].getPotionEffect()) : par1);
    }
    
    public void readFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.readFromNBT(par1NBTTagCompound);
        NBTTagList nbttaglist = par1NBTTagCompound.getTagList("Items");
        this.brewingItemStacks = new ItemStack[this.getSizeInventory()];

        for (int i = 0; i < nbttaglist.tagCount(); ++i)
        {
            NBTTagCompound nbttagcompound1 = (NBTTagCompound)nbttaglist.tagAt(i);
            byte b0 = nbttagcompound1.getByte("Slot");

            if (b0 >= 0 && b0 < this.brewingItemStacks.length)
            {
                this.brewingItemStacks[b0] = ItemStack.loadItemStackFromNBT(nbttagcompound1);
            }
        }

        this.brewTime = par1NBTTagCompound.getShort("BrewTime");

        if (par1NBTTagCompound.hasKey("CustomName"))
        {
            this.field_94132_e = par1NBTTagCompound.getString("CustomName");
        }
    }

    public void writeToNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.writeToNBT(par1NBTTagCompound);
        par1NBTTagCompound.setShort("BrewTime", (short)this.brewTime);
        NBTTagList nbttaglist = new NBTTagList();

        for (int i = 0; i < this.brewingItemStacks.length; ++i)
        {
            if (this.brewingItemStacks[i] != null)
            {
                NBTTagCompound nbttagcompound1 = new NBTTagCompound();
                nbttagcompound1.setByte("Slot", (byte)i);
                this.brewingItemStacks[i].writeToNBT(nbttagcompound1);
                nbttaglist.appendTag(nbttagcompound1);
            }
        }

        par1NBTTagCompound.setTag("Items", nbttaglist);

        if (this.isInvNameLocalized())
        {
            par1NBTTagCompound.setString("CustomName", this.field_94132_e);
        }
    }

    public ItemStack getStackInSlot(int par1)
    {
        return par1 >= 0 && par1 < this.brewingItemStacks.length ? this.brewingItemStacks[par1] : null;
    }

    public ItemStack decrStackSize(int par1, int par2)
    {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length)
        {
            ItemStack itemstack = this.brewingItemStacks[par1];
            this.brewingItemStacks[par1] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    public ItemStack getStackInSlotOnClosing(int par1)
    {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length)
        {
            ItemStack itemstack = this.brewingItemStacks[par1];
            this.brewingItemStacks[par1] = null;
            return itemstack;
        }
        else
        {
            return null;
        }
    }

    public void setInventorySlotContents(int par1, ItemStack par2ItemStack)
    {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length)
        {
            this.brewingItemStacks[par1] = par2ItemStack;
        }
    }

    public int getInventoryStackLimit()
    {
        return 64;
    }

    public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
    {
        return this.worldObj.getBlockTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : par1EntityPlayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }

    public void openChest() {}

    public void closeChest() {}

    public boolean isItemValidForSlot(int par1, ItemStack par2ItemStack)
    {
    	if(par2ItemStack.getItem() instanceof ItemPotion || par2ItemStack.itemID == Item.glassBottle.itemID) 
    		return true;
    	else if(par2ItemStack.getItem() instanceof ItemPotionBase) 
    		return true;
    	else if(PotionHelper2.isElement(par2ItemStack.getItem()))
    		return true;
     	else return false;
    }

    @SideOnly(Side.CLIENT)
    public void setBrewTime(int par1)
    {
        this.brewTime = par1;
    }

    public int getFilledSlots()
    {
        int i = 0;

        for (int j = 0; j < 3; ++j)
        {
            if (this.brewingItemStacks[j] != null)
            {
                i |= 1 << j;
            }
        }

        return i;
    }

    public int[] getAccessibleSlotsFromSide(int par1)
    {
        return par1 == 1 ? ingredientSlot : potionSlots;
    }

    public boolean canInsertItem(int par1, ItemStack par2ItemStack, int par3)
    {
        return this.isItemValidForSlot(par1, par2ItemStack);
    }

    public boolean canExtractItem(int par1, ItemStack par2ItemStack, int par3)
    {
        return true;
    }
}
