package theboo.mods.advancedbrewing.tileentity;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public class TileEntityElectrized extends TileEntity implements ISidedInventory {

    protected ItemStack[] brewingItemStacks;
	
    public TileEntityElectrized(int brewerSize) {
    	brewingItemStacks = new ItemStack[brewerSize];
    }
    
	@Override
	public int getSizeInventory() {
		return 0;
	}

	@Override
	public ItemStack getStackInSlot(int i) {
		return null;
	}

	@Override
	public ItemStack decrStackSize(int i, int j) {
		return null;
	}

	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		return null;
	}

    public void setInventorySlotContents(int par1, ItemStack par2ItemStack) {
        if (par1 >= 0 && par1 < this.brewingItemStacks.length) {
            this.brewingItemStacks[par1] = par2ItemStack;
        }
    }

	@Override
	public String getInvName() {
		return null;
	}

	@Override
	public boolean isInvNameLocalized() {
		return false;
	}
	
	@Override
    public int getInventoryStackLimit()
    {
        return 64;
    }
	
	@Override
    public boolean isUseableByPlayer(EntityPlayer par1EntityPlayer)
    {
        return this.worldObj.getBlockTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : par1EntityPlayer.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D, (double)this.zCoord + 0.5D) <= 64.0D;
    }

	@Override
	public void openChest() {}

	@Override
	public void closeChest() {}

	@Override
	public boolean isItemValidForSlot(int i, ItemStack itemstack) {
		return false;
	}

	@Override
    public boolean canInsertItem(int par1, ItemStack par2ItemStack, int par3) {
    	return this.isItemValidForSlot(par1, par2ItemStack);
    }

	@Override
	public boolean canExtractItem(int i, ItemStack itemstack, int j) {
		return true;
	}

	@Override
	public int[] getAccessibleSlotsFromSide(int var1) {
		return null;
	}
	


}
