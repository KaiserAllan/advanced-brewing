package theboo.mods.advancedbrewing.item;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.block.ModBlocks;
import theboo.mods.advancedbrewing.core.ItemBrew;
import theboo.mods.advancedbrewing.potionapi.ItemPotionBase;
import theboo.mods.advancedbrewing.util.ItemIdReference;
import theboo.mods.advancedbrewing.util.ModInfo;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ModItems {

	public static Item itemReinforcer;
	public static Item consumedSoul;
	public static Item slimeBinded;
	//public static Item itemBinder;
	public static Item itemBrewingStation;
	public static Item itemABrewingStand;
	public static Item magicalDust;
	public static Item crystalQuartz;
	public static ItemPotionBase potion;
	public static Item sulfureo;
	
	public static void init() {
		itemABrewingStand = (new ItemBlockPlacer(ItemIdReference.ITEM_ADVANCED_BREWING_STAND, ModBlocks.aBrewingStand, "aBrewingStand")).setUnlocalizedName(ModInfo.MOD_ID + ":aBrewingStand")
				.setCreativeTab(CreativeTabs.tabBrewing);
		itemBrewingStation = (new ItemBlockPlacer(ItemIdReference.ITEM_BREWING_STATION, ModBlocks.brewingStation, "brewingStation")).setUnlocalizedName(ModInfo.MOD_ID + ":labStation")
				.setCreativeTab(CreativeTabs.tabBrewing);
		potion = (ItemPotionBase) new ItemPotionBase(ItemIdReference.POTION).setUnlocalizedName("potionAdvanced")
				.setCreativeTab(CreativeTabs.tabBrewing);
		slimeBinded = new ItemBrew(ItemIdReference.SLIME_BINDED).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("boundedSlime");
		itemReinforcer = new ItemBlockPlacer(ItemIdReference.ITEM_POTION_REINFORCER, ModBlocks.reinforcer, "reinforcer").setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName(ModInfo.MOD_ID + ":reinforcer");
		consumedSoul = new ItemBrew(ItemIdReference.CONSUMED_SOUL).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("consumedSoul");
		magicalDust = new ItemBrew(ItemIdReference.MAGICAL_DUST).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("magicalDust");
		sulfureo = new ItemBrew(ItemIdReference.SULFUREO).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("sulfureoDust");
		crystalQuartz = new ItemBrew(ItemIdReference.CRYSTAL_QUARTZ).setCreativeTab(CreativeTabs.tabBrewing).setUnlocalizedName("crystalQuartz");
		
		LanguageRegistry.addName(itemABrewingStand, "Advanced Brewing Stand");
		LanguageRegistry.addName(itemBrewingStation, "Brewing Station");
		//LanguageRegistry.addName(itemBinder, "Potion Binder");
		LanguageRegistry.addName(slimeBinded, "Bounded Slimeball");
		LanguageRegistry.addName(itemReinforcer, "Potion Reinforcer");
		LanguageRegistry.addName(consumedSoul, "Consumed Soul");
		LanguageRegistry.addName(magicalDust, "Magical Dust");
		LanguageRegistry.addName(crystalQuartz, "Crystal Quartz");
		LanguageRegistry.addName(sulfureo, "Sulfureo");
	}
	
	public static void addItemRecipes() {
		GameRegistry.addRecipe(new ItemStack(slimeBinded, 1, 0), new Object[] {
			"SSS", "LSL", "SSS", 'L', Item.leash, 'S', Item.slimeBall
		});
		
		GameRegistry.addShapelessRecipe(new ItemStack(consumedSoul, 2, 0), new Object[] {
			Block.obsidian, Item.glowstone, Item.glowstone, Item.glowstone
		});
		
		GameRegistry.addShapelessRecipe(new ItemStack(magicalDust, 2, 0), new Object[] {
			Item.redstone, Item.glowstone, Item.blazePowder
		});
		
		GameRegistry.addShapelessRecipe(new ItemStack(sulfureo, 2, 0), new Object[] {
			magicalDust, Item.gunpowder, Item.blazePowder
		});
		
		GameRegistry.addRecipe(new ItemStack(itemABrewingStand), new Object[] {
			"MDM", "EBE", "RRR", 'M', ModItems.magicalDust, 'E', Item.emerald, 'B', Item.brewingStand, 'R', Item.netherrackBrick
		});
		
		GameRegistry.addRecipe(new ItemStack(itemBrewingStation), new Object[] {
			"MEM", "BBB", "RRR", 'M', ModItems.magicalDust, 'E', Item.emerald, 'B', itemABrewingStand, 'R', Block.netherBrick, 'M', ModItems.magicalDust
		});
		
		GameRegistry.addRecipe(new ItemStack(itemReinforcer), new Object[] {
			"DOD", "DBD", "DOD", 'D', Item.diamond, 'O', Block.obsidian, 'B', ModItems.itemABrewingStand
		});
	}
}