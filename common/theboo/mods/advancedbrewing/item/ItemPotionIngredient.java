package theboo.mods.advancedbrewing.item;

import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.item.Item;
import theboo.mods.advancedbrewing.ingredientapi.IPotionIngredient;
import theboo.mods.advancedbrewing.potionapi.AdvancedPotion;
import theboo.mods.advancedbrewing.util.ModInfo;

/**
 * AdvancedBrewing ItemPotionIngredient
 * 
 * <br>Previosly used as a class that implements IPotionIngredient and extends item.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
@Deprecated
public class ItemPotionIngredient extends Item implements IPotionIngredient {

	public ItemPotionIngredient(int par1) {
		super(par1);
	}

	@Override
	public int getPotion() {
		return 0;
	}

	public void onAdvancedPotionBrewed(AdvancedPotion potion) {
	}

	public void onPotionBrewed() {		
	}
	
	public void registerIcons(IconRegister ir) {
        this.itemIcon = ir.registerIcon(ModInfo.MOD_ID + ":" + (this.getUnlocalizedName().substring(5)));
	}
	
}
