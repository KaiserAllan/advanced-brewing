package theboo.mods.advancedbrewing.entity;

import theboo.mods.advancedbrewing.AdvancedBrewing;
import cpw.mods.fml.common.registry.EntityRegistry;

public class ModEntities {

	public static void init() {
		EntityRegistry.registerModEntity(EntityPotion2.class, "Potion2", 0, AdvancedBrewing.instance, 64, 10, true);
	}
}
