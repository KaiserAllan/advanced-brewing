package theboo.mods.advancedbrewing.entity;

import java.util.Iterator;
import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.projectile.EntityThrowable;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.configuration.ConfigurationLoader;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.potionapi.ItemPotionBase;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.util.handler.idsender.IDHolder;
import cpw.mods.fml.common.registry.IThrowableEntity;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing EntityPotion2
 * 
 * <br>The entity file of the potion. Named with 2 so it won't be confused with vanilla's class.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class EntityPotion2 extends EntityThrowable implements IThrowableEntity
{
    /**
     * The damage value of the thrown potion that this EntityPotion represents.
     */
    private ItemStack potionDamage;
	private Entity shootingEntity;

    public EntityPotion2(World par1World)
    {
        super(par1World);
    }

    public EntityPotion2(World par1World, EntityLivingBase par2EntityLivingBase, int par3)
    {
        this(par1World, par2EntityLivingBase, new ItemStack(ModItems.potion, 1, par3));
    }

    public EntityPotion2(World par1World, EntityLivingBase par2EntityLivingBase, ItemStack par3ItemStack)
    {
        super(par1World, par2EntityLivingBase);
        this.potionDamage = par3ItemStack;
    }

    @SideOnly(Side.CLIENT)
    public EntityPotion2(World par1World, double par2, double par4, double par6, int par8)
    {
        this(par1World, par2, par4, par6, new ItemStack(ModItems.potion, 1, par8));
    }

    public EntityPotion2(World par1World, double par2, double par4, double par6, ItemStack par8ItemStack)
    {
        super(par1World, par2, par4, par6);
        this.potionDamage = par8ItemStack;
    }

    /**
     * Gets the amount of gravity to apply to the thrown entity with each tick.
     */
    protected float getGravityVelocity()
    {
        return 0.05F;
    }

    protected float func_70182_d()
    {
        return 0.5F;
    }

    protected float func_70183_g()
    {
        return -20.0F;
    }

    public void setPotionDamage(int par1)
    {
        if (this.potionDamage == null)
        {
            this.potionDamage = new ItemStack(ModItems.potion, 1, 0);
        }

        this.potionDamage.setItemDamage(par1);
    }

    /**
     * Returns the damage value of the thrown potion that this EntityPotion represents.
     */
    public int getPotionDamage()
    {
        if (this.potionDamage == null)
        {
            this.potionDamage = new ItemStack(ModItems.potion, 1, 0);
        }
        
        return this.potionDamage.getItemDamage();
    }

    /**
     * Called when this EntityThrowable hits a block or entity.
     */
    protected void onImpact(MovingObjectPosition par1MovingObjectPosition)
    {
        if (!this.worldObj.isRemote)
        {
        	ItemPotionBase potion = (ItemPotionBase) potionDamage.getItem();
        	
        	AxisAlignedBB axisalignedbb = this.boundingBox.expand(4.0D, 2.0D, 4.0D);
            List list1 = this.worldObj.getEntitiesWithinAABB(EntityLivingBase.class, axisalignedbb);

            if (list1 != null && !list1.isEmpty())
            {
                Iterator iterator = list1.iterator();

                while (iterator.hasNext())
                {
                    EntityLivingBase entitylivingbase = (EntityLivingBase)iterator.next();
                    double d0 = this.getDistanceSqToEntity(entitylivingbase);

                    if (d0 < 16.0D)
                    {
                        double d1 = 1.0D - Math.sqrt(d0) / 4.0D;

                        if (entitylivingbase == par1MovingObjectPosition.entityHit)
                        {
                            d1 = 1.0D;
                        }

                        //TODO implement multiplie potios
                        PotionEffect[] effects = potion.getPotionFromDamage(potionDamage.getItemDamage()).getEffects();
                        for(int z=0;z<effects.length;z++)
                        {
                        	PotionEffect potioneffect = potion.getPotionFromDamage(potionDamage.getItemDamage()).getEffect(z); 
                        	
                        	int i = potioneffect.getPotionID();

                        	if (Potion.potionTypes[i].isInstant())
                        	{
                        		Potion.potionTypes[i].affectEntity(this.getThrower(), entitylivingbase, potioneffect.getAmplifier(), d1);
                        	}
                        	else
                        	{
                        		int j = (int)(d1 * (double)potioneffect.getDuration() + 0.5D);
                        	
                        		if (j > 20)
                        		{
                        			entitylivingbase.addPotionEffect(new PotionEffect(i, j, potioneffect.getAmplifier()));
                        		}
                        	}
                        }
                    }
                }            
            }
            
            this.worldObj.playAuxSFX(2002, (int)Math.round(this.posX), (int)Math.round(this.posY), (int)Math.round(this.posZ), PotionHelper2.calcColor(IDHolder.getId(), false));
            this.setDead();
        }
    }
    

    /**
     * (abstract) Protected helper method to read subclass entity data from NBT.
     */
    public void readEntityFromNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.readEntityFromNBT(par1NBTTagCompound);

        if (par1NBTTagCompound.hasKey("Potion2"))
        {
            this.potionDamage = ItemStack.loadItemStackFromNBT(par1NBTTagCompound.getCompoundTag("Potion2"));
        }
        else
        {
            this.setPotionDamage(par1NBTTagCompound.getInteger("potionValue"));
        }

        if (this.potionDamage == null)
        {
            this.setDead();
        }
    }

    /**
     * (abstract) Protected helper method to write subclass entity data to NBT.
     */
    public void writeEntityToNBT(NBTTagCompound par1NBTTagCompound)
    {
        super.writeEntityToNBT(par1NBTTagCompound);

        if (this.potionDamage != null)
        {
            par1NBTTagCompound.setCompoundTag("Potion2", this.potionDamage.writeToNBT(new NBTTagCompound()));
        }
    }

    @Override
    public void setThrower(Entity entity) {
    	this.shootingEntity = entity;
    }
}
