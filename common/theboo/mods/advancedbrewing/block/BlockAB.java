package theboo.mods.advancedbrewing.block;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import theboo.mods.advancedbrewing.util.ModInfo;

public class BlockAB extends Block 
{
	public BlockAB(int par1, Material par2Material) {
		super(par1, par2Material);
	}

	public void registerIcons(IconRegister reg) {
		this.blockIcon = reg.registerIcon(ModInfo.MOD_ID + ":" + this.getUnlocalizedName().substring(5));
	}
}
