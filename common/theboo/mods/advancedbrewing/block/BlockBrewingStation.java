package theboo.mods.advancedbrewing.block;

import java.util.List;
import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.AdvancedBrewing;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.proxy.ClientProxy;
import theboo.mods.advancedbrewing.tileentity.TileEntityABrewingStand;
import theboo.mods.advancedbrewing.tileentity.TileEntityBrewingStation;
import theboo.mods.advancedbrewing.util.ModInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing BlockBrewingStation
 * 
 * <br>The block class of the brewing station.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class BlockBrewingStation extends BlockContainer
{
    private Random rand = new Random();
    
    @SideOnly(Side.CLIENT) 
    public Icon connected;

	private boolean primary;
    
    public BlockBrewingStation(int par1)
    {
        super(par1, Material.iron);

    }

    public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
    {
        this.setBlockBounds(-1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);    
        super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
        this.setBlockBoundsForItemRender();
        super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
    }

    public void setBlockBoundsForItemRender()
    {
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);    
    }
    
    public TileEntity createNewTileEntity(World world)
    { 
    	return new TileEntityBrewingStation();
    }

    public boolean isPrimary() {
    	return primary;
    }

    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
    	TileEntityBrewingStation brewingStation = (TileEntityBrewingStation)world.getBlockTileEntity(x, y, z);

        if (world.isRemote) {
            return true;
        }
        else
        {
        	if (brewingStation != null && !player.isSneaking())
            {
            	player.openGui(AdvancedBrewing.instance, 1, world, x, y, z);
            }
        
            return true;
        }
    }

    public void breakBlock(World world, int x, int y, int z, int par5, int par6)
    {
        TileEntity tileentity = world.getBlockTileEntity(x, y, z);
        
        if (tileentity instanceof TileEntityBrewingStation)
        {
            TileEntityBrewingStation brewingStation = (TileEntityBrewingStation)tileentity;

            for (int j1 = 0; j1 < brewingStation.getSizeInventory(); ++j1)
            {
                ItemStack itemstack = brewingStation.getStackInSlot(j1);

                if (itemstack != null)
                {
                    float f = this.rand.nextFloat() * 0.8F + 0.1F;
                    float f1 = this.rand.nextFloat() * 0.8F + 0.1F;
                    float f2 = this.rand.nextFloat() * 0.8F + 0.1F;

                    while (itemstack.stackSize > 0)
                    {
                        int k1 = this.rand.nextInt(21) + 10;

                        if (k1 > itemstack.stackSize)
                        {
                            k1 = itemstack.stackSize;
                        }

                        itemstack.stackSize -= k1;
                        EntityItem entityitem = new EntityItem(world, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(itemstack.itemID, k1, itemstack.getItemDamage()));
                        float f3 = 0.05F;
                        entityitem.motionX = (double)((float)this.rand.nextGaussian() * f3);
                        entityitem.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
                        entityitem.motionZ = (double)((float)this.rand.nextGaussian() * f3);
                        world.spawnEntityInWorld(entityitem);
                    }
                }
            }
        }
        
        super.breakBlock(world, x, y, z, par5, par6);
    }

    public int idDropped(int par1, Random par2Random, int par3)
    {
        return ModItems.itemBrewingStation.itemID;
    }

    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {    	
        double d0 = (double)((float)par2 + 0.4F + par5Random.nextFloat() * 0.2F);
        double d1 = (double)((float)par3 + 0.7F + par5Random.nextFloat() * 0.3F);
        double d2 = (double)((float)par4 + 0.4F + par5Random.nextFloat() * 0.2F);
        par1World.spawnParticle("reddust", d0, d1, d2, 3.0D, 3.0D, 5.25D);
    }

    @SideOnly(Side.CLIENT)
    public int idPicked(World par1World, int par2, int par3, int par4)
    {
        return ModItems.itemBrewingStation.itemID;
    }
    
    public boolean hasComparatorInputOverride()
    {
        return true;
    }

    public int getComparatorInputOverride(World par1World, int par2, int par3, int par4, int par5)
    {
        return Container.calcRedstoneFromInventory((IInventory)par1World.getBlockTileEntity(par2, par3, par4));
    }
    
    public void onBlockPlacedBy(World world, int x, int y, int z, EntityLivingBase par5EntityLivingBase, ItemStack stack)
    {
        int l = MathHelper.floor_double((double)(par5EntityLivingBase.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

        if (l == 0)
        {
            world.setBlockMetadataWithNotify(x, y, z, 2, 2);
        }

        if (l == 1)
        {
            world.setBlockMetadataWithNotify(x, y, z, 5, 2);
        }

        if (l == 2)
        {
        	world.setBlockMetadataWithNotify(x, y, z, 3, 2);
        }

        if (l == 3)
        {
        	world.setBlockMetadataWithNotify(x, y, z, 4, 2);
        }

        if (stack.hasDisplayName())
        {
            ((TileEntityBrewingStation)world.getBlockTileEntity(x, y, z)).setGUIDisplayName(stack.getDisplayName());
        }
        
    }
    
    private void setDefaultDirection(World par1World, int par2, int par3, int par4)
    {
        if (!par1World.isRemote)
        {
            int l = par1World.getBlockId(par2, par3, par4 - 1);
            int i1 = par1World.getBlockId(par2, par3, par4 + 1);
            int j1 = par1World.getBlockId(par2 - 1, par3, par4);
            int k1 = par1World.getBlockId(par2 + 1, par3, par4);
            byte b0 = 3;

            if (Block.opaqueCubeLookup[l] && !Block.opaqueCubeLookup[i1])
            {
                b0 = 3;
            }

            if (Block.opaqueCubeLookup[i1] && !Block.opaqueCubeLookup[l])
            {
                b0 = 2;
            }

            if (Block.opaqueCubeLookup[j1] && !Block.opaqueCubeLookup[k1])
            {
                b0 = 5;
            }

            if (Block.opaqueCubeLookup[k1] && !Block.opaqueCubeLookup[j1])
            {
                b0 = 4;
            }

            par1World.setBlockMetadataWithNotify(par2, par3, par4, b0, 2);
        }
    }
    
    public boolean isOpaqueCube() {
    	return false;
    }

    public boolean renderAsNormalBlock() {
        return false;
    }
    
    @Override
    public int getRenderType() {
    	return -1;
    }
    
    @SideOnly(Side.CLIENT)

    /**
     * Returns the bounding box of the wired rectangular prism to render.
     */
    public AxisAlignedBB getSelectedBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        this.setBlockBoundsBasedOnState(par1World, par2, par3, par4);
        return super.getSelectedBoundingBoxFromPool(par1World, par2, par3, par4);
    }

    public AxisAlignedBB getCollisionBoundingBoxFromPool(World par1World, int par2, int par3, int par4)
    {
        this.setBlockBoundsBasedOnState(par1World, par2, par3, par4);
        return super.getCollisionBoundingBoxFromPool(par1World, par2, par3, par4);
    }

    public void setBlockBoundsBasedOnState(IBlockAccess blockaccess, int x, int y, int z)
    {
    	switch(getConnnectedSide(blockaccess, x, y, z)) {
    	case 0:
            this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);    
    	case 1:
            this.setBlockBounds(-1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);    
    	case 2:
            this.setBlockBounds(1.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);    
    	case 3:
            this.setBlockBounds(0.0F, 0.0F, -1.0F, 1.0F, 1.0F, 1.0F);  
    	case 4:
            this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 1.0F, 1.0F);
    	}
    }
    
    public MovingObjectPosition collisionRayTrace(World par1World, int par2, int par3, int par4, Vec3 par5Vec3, Vec3 par6Vec3)
    {
        this.setBlockBoundsBasedOnState(par1World, par2, par3, par4);
        return super.collisionRayTrace(par1World, par2, par3, par4, par5Vec3, par6Vec3);
    }
    
    public boolean isConnected(World world, int x, int y, int z) {
        if(world.getBlockId(x - 1, y, z) == this.blockID) 
        	return true;  
        else if(world.getBlockId(x + 1, y, z) == this.blockID) 
        	return true;
        else if(world.getBlockId(x, y, z - 1) == this.blockID) 
        	return true;
        else if(world.getBlockId(x, y, z + 1) == this.blockID) 
        	return true;
        else return false;
    }
    
    public boolean isConnected(IBlockAccess blockaccess, int x, int y, int z) {
        if(blockaccess.getBlockId(x - 1, y, z) == this.blockID) 
        	return true;  
        else if(blockaccess.getBlockId(x + 1, y, z) == this.blockID) 
        	return true;
        else if(blockaccess.getBlockId(x, y, z - 1) == this.blockID) 
        	return true;
        else if(blockaccess.getBlockId(x, y, z + 1) == this.blockID) 
        	return true;
        else return false;
    }
    
    public int getConnnectedSide(IBlockAccess blockaccess, int x, int y, int z) {
    	if(isConnected(blockaccess, x, y, z))  {
            if(blockaccess.getBlockId(x - 1, y, z) == this.blockID) 
            	return 1;  
            else if(blockaccess.getBlockId(x + 1, y, z) == this.blockID) 
            	return 2;
            else if(blockaccess.getBlockId(x, y, z - 1) == this.blockID) 
            	return 3;
            else if(blockaccess.getBlockId(x, y, z + 1) == this.blockID) 
            	return 4;
    	}
		return 0;
    }
    
    public int getConnnectedSide(World world, int x, int y, int z) {
    	if(isConnected(world, x, y, z))  {
            if(world.getBlockId(x - 1, y, z) == this.blockID) 
            	return 1;  
            else if(world.getBlockId(x + 1, y, z) == this.blockID) 
            	return 2;
            else if(world.getBlockId(x, y, z - 1) == this.blockID) 
            	return 3;
            else if(world.getBlockId(x, y, z + 1) == this.blockID) 
            	return 4;
    	}
		return 0;
    }
    
    public void registerIcons(IconRegister reg) {
    	reg.registerIcon(ModInfo.MOD_ID + ":brewing_station");
    }
}
