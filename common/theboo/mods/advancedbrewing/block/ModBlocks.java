package theboo.mods.advancedbrewing.block;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.util.BlockIdReference;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

public class ModBlocks {

	public static Block aBrewingStand;
	public static Block brewingStation;
	public static Block binder;
	public static Block reinforcer;
	public static Block crystalQuartzOre;

	
	public static void init() {
		aBrewingStand = new BlockABrewingStand(BlockIdReference.ADVANCED_BREWING_STAND).setHardness(0.5F).setLightValue(0.125F).setUnlocalizedName("aBrewingStand");
		brewingStation = new BlockBrewingStation(BlockIdReference.BREWING_STATION).setHardness(0.5F).setLightValue(0.125F).setUnlocalizedName("labStation");
		binder = new BlockPotionBinder(BlockIdReference.POTION_BINDER).setHardness(0.5F).setLightValue(0.125F).setUnlocalizedName("binder");
		reinforcer = new BlockPotionReinforcer(BlockIdReference.POTION_REINFORCER).setHardness(0.5F).setLightValue(0.125F).setUnlocalizedName("reinforcer");
	    crystalQuartzOre = new BlockOre(BlockIdReference.CRYSTAL_QUARTZ_ORE).setHardness(3.0F).setResistance(5.0F).setStepSound(Block.soundStoneFootstep).setUnlocalizedName("crystalQuartzOre");

		GameRegistry.registerBlock(aBrewingStand, "aBrewingStand");
		GameRegistry.registerBlock(brewingStation, "Lab Station");
		GameRegistry.registerBlock(binder, "Binder");
		GameRegistry.registerBlock(reinforcer, "Reinforcer");
		GameRegistry.registerBlock(crystalQuartzOre, "crystalQuartzOre");

		LanguageRegistry.addName(aBrewingStand, "Advanced Brewing Stand");
		LanguageRegistry.addName(reinforcer, "Potion Reinforcer");
		LanguageRegistry.addName(brewingStation, "Brewing Station");
		LanguageRegistry.addName(binder, "Potion Binder");
		LanguageRegistry.addName(crystalQuartzOre, "Crystal Quartz Ore");
	
	}
	
	public static void addBlockRecipes() {
		GameRegistry.addRecipe(new ItemStack(binder), new Object[] {
			"MDM", "BSB", "BCB", 'M', ModItems.magicalDust, 'D', Item.diamond, 'B', ModItems.itemABrewingStand, 'S', Item.slimeBall, 'C', Item.cauldron
		});
	}
}
