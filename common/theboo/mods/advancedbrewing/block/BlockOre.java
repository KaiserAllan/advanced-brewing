package theboo.mods.advancedbrewing.block;

import java.util.Random;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.item.ModItems;

public class BlockOre extends BlockAB
{
    public BlockOre(int par1)
    {
        super(par1, Material.rock);
        this.setCreativeTab(CreativeTabs.tabBlock);
    }

    public int idDropped(int par1, Random par2Random, int par3){
        return this.blockID == ModBlocks.crystalQuartzOre.blockID ? ModItems.crystalQuartz.itemID : this.blockID;
    }
    
    public int quantityDroppedWithBonus(int par1, Random par2Random)
    {
        if (par1 > 0 && this.blockID != this.idDropped(0, par2Random, par1)) {
            int j = par2Random.nextInt(par1 + 2) - 1;

            if (j < 0) {
                j = 0;
            }

            return this.quantityDropped(par2Random) * (j + 1);
        }
        else
        {
            return this.quantityDropped(par2Random);
        }
    }
    
    public int quantityDropped(Random par1Random){
        return this.blockID == ModBlocks.crystalQuartzOre.blockID ? 1 + par1Random.nextInt(2) : 1;
    }

    public void dropBlockAsItemWithChance(World par1World, int par2, int par3, int par4, int par5, float par6, int par7)
    {
        super.dropBlockAsItemWithChance(par1World, par2, par3, par4, par5, par6, par7);

        if (this.idDropped(par5, par1World.rand, par7) != this.blockID) {
            int amount = 0;

            if (this.blockID == Block.oreNetherQuartz.blockID) {
                amount = MathHelper.getRandomIntegerInRange(par1World.rand, 2, 5);
            }

            this.dropXpOnBlockBreak(par1World, par2, par3, par4, amount);
        }
    }
}
