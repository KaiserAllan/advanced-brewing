package theboo.mods.advancedbrewing.block;

import java.util.List;
import java.util.Random;

import net.minecraft.block.BlockBrewingStand;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import theboo.mods.advancedbrewing.AdvancedBrewing;
import theboo.mods.advancedbrewing.item.ModItems;
import theboo.mods.advancedbrewing.tileentity.TileEntityABrewingStand;
import theboo.mods.advancedbrewing.util.ModInfo;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing BlockABrewingStand
 * 
 * <br>The block class of the advanced brewing stand.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class BlockABrewingStand extends BlockBrewingStand
{
    private Random rand = new Random();
    @SideOnly(Side.CLIENT)
    private Icon theIcon;

    public BlockABrewingStand(int par1)
    {
        super(par1);
    }

    public boolean isOpaqueCube()
    {
        return false;
    }

    public int getRenderType()
    {
        return 25;
    }

    public TileEntity createNewTileEntity(World par1World)
    {
        return new TileEntityABrewingStand();
    }
    
    public boolean renderAsNormalBlock()
    {
        return false;
    }

    public void addCollisionBoxesToList(World par1World, int par2, int par3, int par4, AxisAlignedBB par5AxisAlignedBB, List par6List, Entity par7Entity)
    {
        this.setBlockBounds(0.4375F, 0.0F, 0.4375F, 0.5625F, 0.875F, 0.5625F);
        super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
        this.setBlockBoundsForItemRender();
        super.addCollisionBoxesToList(par1World, par2, par3, par4, par5AxisAlignedBB, par6List, par7Entity);
    }

    public void setBlockBoundsForItemRender()
    {
        this.setBlockBounds(0.0F, 0.0F, 0.0F, 1.0F, 0.125F, 1.0F);
    }

    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9)
    {
        TileEntityABrewingStand TileEntityABrewingStand = (TileEntityABrewingStand)world.getBlockTileEntity(x, y, z);

        if (world.isRemote)
        {
            return true;
        }
        else
        {

            if (TileEntityABrewingStand != null && !player.isSneaking())
            {
            	player.openGui(AdvancedBrewing.instance, 0, world, x, y, z);
            }

            return true;
        }
    }

    public void onBlockPlacedBy(World par1World, int par2, int par3, int par4, EntityLivingBase par5EntityLivingBase, ItemStack par6ItemStack)
    {
        par1World.notifyBlocksOfNeighborChange(par2, par3, par4, this.blockID);
        par1World.notifyBlocksOfNeighborChange(par2 - 1, par3, par4, this.blockID);
        par1World.notifyBlocksOfNeighborChange(par2 + 1, par3, par4, this.blockID);
        par1World.notifyBlocksOfNeighborChange(par2, par3, par4 - 1, this.blockID);
        par1World.notifyBlocksOfNeighborChange(par2, par3, par4 + 1, this.blockID);
        
        if (par6ItemStack.hasDisplayName())
        {
            ((TileEntityABrewingStand)par1World.getBlockTileEntity(par2, par3, par4)).func_94131_a(par6ItemStack.getDisplayName());
        }
    }

    public void breakBlock(World par1World, int par2, int par3, int par4, int par5, int par6)
    {
        TileEntity tileentity = par1World.getBlockTileEntity(par2, par3, par4);

        if (tileentity instanceof TileEntityABrewingStand)
        {
            TileEntityABrewingStand TileEntityABrewingStand = (TileEntityABrewingStand)tileentity;

            for (int j1 = 0; j1 < TileEntityABrewingStand.getSizeInventory(); ++j1)
            {
                ItemStack itemstack = TileEntityABrewingStand.getStackInSlot(j1);

                if (itemstack != null)
                {
                    float f = this.rand.nextFloat() * 0.8F + 0.1F;
                    float f1 = this.rand.nextFloat() * 0.8F + 0.1F;
                    float f2 = this.rand.nextFloat() * 0.8F + 0.1F;

                    while (itemstack.stackSize > 0)
                    {
                        int k1 = this.rand.nextInt(21) + 10;

                        if (k1 > itemstack.stackSize)
                        {
                            k1 = itemstack.stackSize;
                        }

                        itemstack.stackSize -= k1;
                        EntityItem entityitem = new EntityItem(par1World, (double)((float)par2 + f), (double)((float)par3 + f1), (double)((float)par4 + f2), new ItemStack(itemstack.itemID, k1, itemstack.getItemDamage()));
                        float f3 = 0.05F;
                        entityitem.motionX = (double)((float)this.rand.nextGaussian() * f3);
                        entityitem.motionY = (double)((float)this.rand.nextGaussian() * f3 + 0.2F);
                        entityitem.motionZ = (double)((float)this.rand.nextGaussian() * f3);
                        par1World.spawnEntityInWorld(entityitem);
                    }
                }
            }
        }

        super.breakBlock(par1World, par2, par3, par4, par5, par6);
    }

    public int idDropped(int par1, Random par2Random, int par3)
    {
        return ModItems.itemABrewingStand.itemID;
    }

    @SideOnly(Side.CLIENT)
    public void randomDisplayTick(World par1World, int par2, int par3, int par4, Random par5Random)
    {
        double d0 = (double)((float)par2 + 0.4F + par5Random.nextFloat() * 0.2F);
        double d1 = (double)((float)par3 + 0.7F + par5Random.nextFloat() * 0.3F);
        double d2 = (double)((float)par4 + 0.4F + par5Random.nextFloat() * 0.2F);
        par1World.spawnParticle("reddust", d0, d1, d2, 0.0D, 3.0D, 0.25D);
    }

    @SideOnly(Side.CLIENT)
    public int idPicked(World par1World, int par2, int par3, int par4)
    {
        return ModItems.itemABrewingStand.itemID;
    }
    
    public boolean hasComparatorInputOverride()
    {
        return true;
    }

    public int getComparatorInputOverride(World par1World, int par2, int par3, int par4, int par5)
    {
        return Container.calcRedstoneFromInventory((IInventory)par1World.getBlockTileEntity(par2, par3, par4));
    }

    @SideOnly(Side.CLIENT)
    public void registerIcons(IconRegister par1IconRegister)
    {
        this.theIcon = par1IconRegister.registerIcon(ModInfo.MOD_ID + ":aBrewingStand" + "_base");
        this.blockIcon = par1IconRegister.registerIcon(ModInfo.MOD_ID + ":aBrewingStand");
    }

    @SideOnly(Side.CLIENT)
    public Icon getBrewingStandIcon()
    {
        return this.theIcon;
    }
    
    public Icon getItemIcon() {
		return blockIcon;
    }
}
