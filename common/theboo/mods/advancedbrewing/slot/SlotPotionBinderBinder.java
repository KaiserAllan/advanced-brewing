package theboo.mods.advancedbrewing.slot;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.container.ContainerPotionBinder;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;

/**
 * AdvancedBrewing SlotPotionBinderBinder
 * 
 * <br> Slot file for ingredients.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class SlotPotionBinderBinder extends Slot
{
    final ContainerPotionBinder binder;

    public SlotPotionBinderBinder(ContainerPotionBinder binder, IInventory par2IInventory, int par3, int par4, int par5)
    {
        super(par2IInventory, par3, par4, par5);
        this.binder = binder;
    }

    public boolean isItemValid(ItemStack par1ItemStack)
    {
    	if(par1ItemStack != null) {
    		if(PotionHelper2.isBinder(par1ItemStack.getItem()))
    			return true;
    	}
		return false;
    }

    public int getSlotStackLimit()
    {
        return 64;
    }
}
