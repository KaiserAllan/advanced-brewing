package theboo.mods.advancedbrewing.slot;

import theboo.mods.advancedbrewing.potionapi.ItemPotionBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemPotion;
import net.minecraft.item.ItemStack;
import net.minecraft.stats.AchievementList;

/**
 * AdvancedBrewing SlotABrewingStandPotion
 * 
 * <br> Slot file for potions.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class SlotUniversalPotion extends Slot
{
    /** The player that has this container open. */
    private EntityPlayer player;

    public SlotUniversalPotion(EntityPlayer par1EntityPlayer, IInventory par2IInventory, int par3, int par4, int par5)
    {
        super(par2IInventory, par3, par4, par5);
        this.player = par1EntityPlayer;
    }
    
    public boolean isItemValid(ItemStack par1ItemStack)
    {
        return canHoldPotion(par1ItemStack);
    }

    public int getSlotStackLimit()
    {
        return 1;
    }

    public void onPickupFromSlot(EntityPlayer par1EntityPlayer, ItemStack par2ItemStack)
    {
        if (par2ItemStack.getItem() instanceof ItemPotion && par2ItemStack.getItemDamage() > 0)
        {
            this.player.addStat(AchievementList.potion, 1);
        }

        super.onPickupFromSlot(par1EntityPlayer, par2ItemStack);
    }

    public static boolean canHoldPotion(ItemStack par0ItemStack)
    {
    	if(par0ItemStack != null) {
    		if(par0ItemStack.getItem() instanceof ItemPotion || par0ItemStack.itemID == Item.glassBottle.itemID) {
    			return true;
    		}
    		else if(par0ItemStack.getItem() instanceof ItemPotionBase) {
    			return true;
    		}
    	}
		return false;
    }
}
