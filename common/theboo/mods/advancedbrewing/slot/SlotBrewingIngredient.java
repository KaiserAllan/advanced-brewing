package theboo.mods.advancedbrewing.slot;

import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.container.ContainerBrewer;
import theboo.mods.advancedbrewing.ingredientapi.IPotionIngredient;
import theboo.mods.advancedbrewing.item.ItemPotionIngredient;
import theboo.mods.advancedbrewing.potionapi.util.PotionHelper2;
import theboo.mods.advancedbrewing.util.handler.packets.PacketHandler;
import cpw.mods.fml.client.FMLClientHandler;
import cpw.mods.fml.common.FMLCommonHandler;

/**
 * AdvancedBrewing SlotBrewingStationIngredient
 * 
 * <br> Slot file for ingredients.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */
public class SlotBrewingIngredient extends Slot
{
    /** The brewing stand this slot belongs to. */
    final ContainerBrewer container;

    public SlotBrewingIngredient(ContainerBrewer container, IInventory par2IInventory, int par3, int par4, int par5)
    {
        super(par2IInventory, par3, par4, par5);
        this.container = container;
    }

    public boolean isItemValid(ItemStack stack)
    {
    	//PacketHandler.sendValidCheckPacket(stack, getSlotIndex(), container.getTileBrewer().xCoord, container.getTileBrewer().yCoord, container.getTileBrewer().zCoord, container.getInvPlayer().player.worldObj.provider.dimensionId);
    	System.out.println("Yo"+FMLCommonHandler.instance().getSide());

    	if(stack != null) {//!container.getInvPlayer().player.worldObj.isRemote) { 
    		boolean valid = false;
    		
    		if(Item.itemsList[stack.itemID].isPotionIngredient(stack)) {
    			valid = true;
    		}
    		else if(stack.getItem() instanceof IPotionIngredient || stack.getItem() instanceof ItemPotionIngredient) {
    			valid = true;
    		}
    		else if(PotionHelper2.isIngredient(stack.getItemDamage(), stack.getItem())) {
    			valid = true;
    		}
    		
    		System.out.println("Ey"+valid);
    		//PacketHandler.sendItemPacket(par1ItemStack, getSlotIndex(), valid, container.getTileBrewer().xCoord, container.getTileBrewer().yCoord, container.getTileBrewer().zCoord, container.getInvPlayer().player.worldObj.provider.dimensionId);    		
    		return valid;
    	}
    	
    	
		//PacketHandler.sendItemPacket(par1ItemStack, getSlotIndex(), false, container.getTileBrewer().xCoord, container.getTileBrewer().yCoord, container.getTileBrewer().zCoord, container.getInvPlayer().player.worldObj.provider.dimensionId);    		
    	
		return false;
    }

    public int getSlotStackLimit()
    {
        return 64;
    }
}
