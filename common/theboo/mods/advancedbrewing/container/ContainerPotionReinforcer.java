package theboo.mods.advancedbrewing.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.slot.SlotBrewingIngredient;
import theboo.mods.advancedbrewing.slot.SlotPotionElement;
import theboo.mods.advancedbrewing.slot.SlotUniversalPotion;
import theboo.mods.advancedbrewing.tileentity.TileEntityPotionReinforcer;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing ContainerPotionReinforcer
 * 
 * The container class of the potion reinforcer.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */

public class ContainerPotionReinforcer extends ContainerBrewer
{
    private TileEntityPotionReinforcer reinforcer;

    private final Slot theSlot;
    private final Slot theOtherSlot;
    private int brewTime;

    public ContainerPotionReinforcer(InventoryPlayer invPlayer, TileEntityPotionReinforcer reinforcer) {    	
    	super(invPlayer, reinforcer);
    	
        this.reinforcer = reinforcer;
        this.addSlotToContainer(new SlotUniversalPotion(invPlayer.player, reinforcer, 0, 79, 54));

        this.theSlot = this.addSlotToContainer(new SlotBrewingIngredient(this, reinforcer, 1, 79, 17));
        this.theOtherSlot = this.addSlotToContainer(new SlotPotionElement(this, reinforcer, 2, 58, 45));

        int i;

        for (i = 0; i < 3; ++i) {
            for (int j = 0; j < 9; ++j){
                this.addSlotToContainer(new Slot(invPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)  {
            this.addSlotToContainer(new Slot(invPlayer, i, 8 + i * 18, 142));
        }
    }

    public void addCraftingToCrafters(ICrafting par1ICrafting)
    {
        super.addCraftingToCrafters(par1ICrafting);
        par1ICrafting.sendProgressBarUpdate(this, 0, this.reinforcer.getBrewTime());
    }

    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);

            if (this.brewTime != this.reinforcer.getBrewTime())
            {
                icrafting.sendProgressBarUpdate(this, 0, this.reinforcer.getBrewTime());
            }
        }

        this.brewTime = this.reinforcer.getBrewTime();
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int par1, int par2)
    {
        if (par1 == 0)
        {
            this.reinforcer.setBrewTime(par2);
        }
    }

    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.reinforcer.isUseableByPlayer(par1EntityPlayer);
    }
    
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slotIndex)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotIndex);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if ((slotIndex < 0 || slotIndex > 1) && slotIndex != 2)
            {
                if ((!this.theSlot.getHasStack() && ((SlotBrewingIngredient)theSlot).isItemValid(itemstack1)))
                {
                    if (!this.mergeItemStack(itemstack1, 1, 2, false))
                    {
                        return null;
                    }
                }
                else if ((!this.theOtherSlot.getHasStack() && ((SlotPotionElement)theOtherSlot).isItemValid(itemstack1)))
                {
                    if (!this.mergeItemStack(itemstack1, 2, 3, false))
                    {
                        return null;
                    }
                }
                else if (SlotUniversalPotion.canHoldPotion(itemstack))
                {
                    if (!this.mergeItemStack(itemstack1, 0, 1, false))
                    {
                        return null;
                    }
                }
                else if (slotIndex >= 3 && slotIndex < 30)
                {
                    if (!this.mergeItemStack(itemstack1, 30, 39, false))
                    {
                        return null;
                    }
                }
                else if (slotIndex >= 30 && slotIndex < 39)
                {
                    if (!this.mergeItemStack(itemstack1, 3, 30, false))
                    {
                        return null;
                    }
                }
                else if (!this.mergeItemStack(itemstack1, 3, 39, false))
                {
                    return null;
                }
            }
            else
            {
                if (!this.mergeItemStack(itemstack1, 3, 39, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }

        return itemstack;
    }
}
