package theboo.mods.advancedbrewing.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import theboo.mods.advancedbrewing.tileentity.TileEntityElectrized;

public class ContainerBrewer extends Container {

	protected TileEntityElectrized tileBrewer;
	protected InventoryPlayer invPlayer;

	@Override
	public boolean canInteractWith(EntityPlayer entityplayer) {
		return false;
	}

    public ContainerBrewer(InventoryPlayer invPlayer, TileEntityElectrized brewer) {    	
        this.tileBrewer = brewer;
        this.invPlayer = invPlayer;
    }

	public TileEntityElectrized getTileBrewer() {
		return tileBrewer;
	}

	public void setTileBrewer(TileEntityElectrized tileBrewer) {
		this.tileBrewer = tileBrewer;
	}

	public InventoryPlayer getInvPlayer() {
		return invPlayer;
	}

	public void setInvPlayer(InventoryPlayer invPlayer) {
		this.invPlayer = invPlayer;
	}
}
