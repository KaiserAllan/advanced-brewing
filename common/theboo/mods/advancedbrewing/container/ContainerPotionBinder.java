package theboo.mods.advancedbrewing.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.slot.SlotPotionBinderBinder;
import theboo.mods.advancedbrewing.slot.SlotPotionElement;
import theboo.mods.advancedbrewing.slot.SlotUniversalPotion;
import theboo.mods.advancedbrewing.tileentity.TileEntityPotionBinder;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing ContainerPotionBinder
 * 
 * The container class of the potion binder.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */

public class ContainerPotionBinder extends Container
{
    private TileEntityPotionBinder theBinder;

    private final Slot theSlot;
    private final Slot theOtherSlot;
    private int brewTime;

    public ContainerPotionBinder(InventoryPlayer par1InventoryPlayer, TileEntityPotionBinder binder)
    {    	
        this.theBinder = binder;
        this.addSlotToContainer(new SlotUniversalPotion(par1InventoryPlayer.player, binder, 0, 57, 15));
        this.addSlotToContainer(new SlotUniversalPotion(par1InventoryPlayer.player, binder, 1, 101, 15));
        this.addSlotToContainer(new SlotUniversalPotion(par1InventoryPlayer.player, binder, 2, 79, 57));

        this.theSlot = this.addSlotToContainer(new SlotPotionElement(this, binder, 3, 38, 57));
        this.theOtherSlot = this.addSlotToContainer(new SlotPotionBinderBinder(this, binder, 4, 79, 8));

        int i;

        for (i = 0; i < 3; ++i) {
            for (int j = 0; j < 9; ++j){
                this.addSlotToContainer(new Slot(par1InventoryPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)  {
            this.addSlotToContainer(new Slot(par1InventoryPlayer, i, 8 + i * 18, 142));
        }
    }

    public void addCraftingToCrafters(ICrafting par1ICrafting)
    {
        super.addCraftingToCrafters(par1ICrafting);
        par1ICrafting.sendProgressBarUpdate(this, 0, this.theBinder.getBrewTime());
    }

    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);

            if (this.brewTime != this.theBinder.getBrewTime())
            {
                icrafting.sendProgressBarUpdate(this, 0, this.theBinder.getBrewTime());
            }
        }

        this.brewTime = this.theBinder.getBrewTime();
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int par1, int par2)
    {
        if (par1 == 0)
        {
            this.theBinder.setBrewTime(par2);
        }
    }

    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.theBinder.isUseableByPlayer(par1EntityPlayer);
    }
    
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slotIndex)
    {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotIndex);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if ((slotIndex < 0 || slotIndex > 3) && slotIndex != 4)
            {
                if ((!this.theSlot.getHasStack() && ((SlotPotionElement)theSlot).isItemValid(itemstack1)))
                {
                    if (!this.mergeItemStack(itemstack1, 3, 4, false))
                    {
                        return null;
                    }
                }
                else if ((!this.theOtherSlot.getHasStack() && ((SlotPotionBinderBinder)theOtherSlot).isItemValid(itemstack1)))
                {
                    if (!this.mergeItemStack(itemstack1, 4, 5, false))
                    {
                        return null;
                    }
                }
                else if (SlotUniversalPotion.canHoldPotion(itemstack))
                {
                    if (!this.mergeItemStack(itemstack1, 0, 3, false))
                    {
                        return null;
                    }
                }
                else if (slotIndex >= 5 && slotIndex < 32)
                {
                    if (!this.mergeItemStack(itemstack1, 32, 41, false))
                    {
                        return null;
                    }
                }
                else if (slotIndex >= 32 && slotIndex < 41)
                {
                    if (!this.mergeItemStack(itemstack1, 5, 32, false))
                    {
                        return null;
                    }
                }
                else if (!this.mergeItemStack(itemstack1, 5, 41, false))
                {
                    return null;
                }
            }
            else
            {
                if (!this.mergeItemStack(itemstack1, 5, 41, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }

        return itemstack;
    }
}
