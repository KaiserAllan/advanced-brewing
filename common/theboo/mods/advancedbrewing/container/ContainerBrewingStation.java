package theboo.mods.advancedbrewing.container;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import theboo.mods.advancedbrewing.slot.SlotBrewingIngredient;
import theboo.mods.advancedbrewing.slot.SlotUniversalPotion;
import theboo.mods.advancedbrewing.tileentity.TileEntityBrewingStation;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

/**
 * AdvancedBrewing ContainerBrewingStation
 * 
 * The container class of the brewing station.
 * 
 * @license 
    Copyright (C) 2013 TheBoo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * @author TheBoo
 *   
 */

public class ContainerBrewingStation extends ContainerBrewer
{
    private TileEntityBrewingStation brewingStation;

    private final Slot theSlot;
    private int brewTime;

    public ContainerBrewingStation(InventoryPlayer invPlayer, TileEntityBrewingStation brewer) {    	
    	super(invPlayer, brewer);
    	
        this.brewingStation = brewer;
        this.addSlotToContainer(new SlotUniversalPotion(invPlayer.player, brewer, 0, 37, 54));
        this.addSlotToContainer(new SlotUniversalPotion(invPlayer.player, brewer, 1, 57, 54));
        this.addSlotToContainer(new SlotUniversalPotion(invPlayer.player, brewer, 2, 79, 54));
        this.addSlotToContainer(new SlotUniversalPotion(invPlayer.player, brewer, 3, 102, 54));
        this.addSlotToContainer(new SlotUniversalPotion(invPlayer.player, brewer, 4, 122, 54));

        this.theSlot = this.addSlotToContainer(new SlotBrewingIngredient(this, brewer, 5, 79, 17));
        int i;

        for (i = 0; i < 3; ++i) {
            for (int j = 0; j < 9; ++j){
                this.addSlotToContainer(new Slot(invPlayer, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
            }
        }

        for (i = 0; i < 9; ++i)  {
            this.addSlotToContainer(new Slot(invPlayer, i, 8 + i * 18, 142));
        }
    }

    public void addCraftingToCrafters(ICrafting par1ICrafting)
    {
        super.addCraftingToCrafters(par1ICrafting);
        par1ICrafting.sendProgressBarUpdate(this, 0, this.brewingStation.getBrewTime());
    }

    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        for (int i = 0; i < this.crafters.size(); ++i)
        {
            ICrafting icrafting = (ICrafting)this.crafters.get(i);

            if (this.brewTime != this.brewingStation.getBrewTime())
            {
                icrafting.sendProgressBarUpdate(this, 0, this.brewingStation.getBrewTime());
            }
        }

        this.brewTime = this.brewingStation.getBrewTime();
    }

    @SideOnly(Side.CLIENT)
    public void updateProgressBar(int par1, int par2)
    {
        if (par1 == 0)
        {
            this.brewingStation.setBrewTime(par2);
        }
    }

    public boolean canInteractWith(EntityPlayer par1EntityPlayer)
    {
        return this.brewingStation.isUseableByPlayer(par1EntityPlayer);
    }
    
    public ItemStack transferStackInSlot(EntityPlayer par1EntityPlayer, int slotIndex)  {
        ItemStack itemstack = null;
        Slot slot = (Slot)this.inventorySlots.get(slotIndex);

        if (slot != null && slot.getHasStack())
        {
            ItemStack itemstack1 = slot.getStack();
            itemstack = itemstack1.copy();

            if ((slotIndex < 0 || slotIndex > 4) && slotIndex != 5)
            {
                if (!this.theSlot.getHasStack() && ((SlotBrewingIngredient)theSlot).isItemValid(itemstack1))
                {
                    if (!this.mergeItemStack(itemstack1, 5, 6, false)) {
                        return null;
                    }
                }
                else if (SlotUniversalPotion.canHoldPotion(itemstack))
                {
                    if (!this.mergeItemStack(itemstack1, 0, 5, false))
                    {
                        return null;
                    }
                }
                else if (slotIndex >= 6 && slotIndex < 33)
                {
                    if (!this.mergeItemStack(itemstack1, 33, 42, false))
                    {
                        return null;
                    }
                }
                else if (slotIndex >= 33 && slotIndex < 42)
                {
                    if (!this.mergeItemStack(itemstack1, 6, 33, false))
                    {
                        return null;
                    }
                }
                else if (!this.mergeItemStack(itemstack1, 6, 42, false))
                {
                    return null;
                }
            }
            else
            {
                if (!this.mergeItemStack(itemstack1, 6, 42, true))
                {
                    return null;
                }

                slot.onSlotChange(itemstack1, itemstack);
            }

            if (itemstack1.stackSize == 0)
            {
                slot.putStack((ItemStack)null);
            }
            else
            {
                slot.onSlotChanged();
            }

            if (itemstack1.stackSize == itemstack.stackSize)
            {
                return null;
            }

            slot.onPickupFromSlot(par1EntityPlayer, itemstack1);
        }

        return itemstack;
    }
}
